#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 23.08.20
Author: Tomy Hommel @TUD
"""

import io
import os
import zipfile
import requests
from modules.path_of_dataset import source

class Directory:

    def __init__(self, root=None):

        if root == None:
            self.path = source
        else:
            self.path = root

    def set_root_directory(self, root):
        self.path = root

    def check_dir(self):
        return os.path.isdir(self.path)

    def list_files(self):
        count = 0
        list_of_file_paths = list()
        list_of_file_names = list()

        for path, subdirs, files in os.walk(self.path):

            for file in files:

                if file.endswith('.mat'):
                    count += 1
                    absolute_path = path+"/"+file
                    list_of_file_paths.append(absolute_path)
                    file = file[:-4]
                    list_of_file_names.append(file)


        if count == 0:
            print('no battery-data found')
            self.download_sample_data()

            for path, subdirs, files in os.walk(self.path):

                for file in files:

                    if file.endswith('.mat'):
                        count += 1
                        absolute_path = path + "/" + file
                        list_of_file_paths.append(absolute_path)
                        file = file[:-4]
                        list_of_file_names.append(file)

        message = ("\n"+str(count)+" files found"+"\n")
        return sorted(list_of_file_paths), sorted(list_of_file_names), message

    def get_file_path(self, filename):

        for path, subdirs, files in os.walk(self.path):
            for file in files:
                if file == filename:
                    return path+"/"+file


    def download_sample_data(self):
        print('try to download data - please wait!')
        url = "https://ti.arc.nasa.gov/c/5/"
        response = requests.get(url)
        print('http-request: ', response)  # 200 -> OK
        zip = zipfile.ZipFile(io.BytesIO(response.content))
        zip.extractall(path=self.path + "/battery_data")


if __name__ == "__main__":

    root = Directory()

    paths, names, message = root.list_files()
    print(names)
    print(message)

