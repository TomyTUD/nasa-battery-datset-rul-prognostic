#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26.06.20
Author: Tomy Hommel @TUD
"""

import numpy as np
import scipy.io as sio
from datetime import datetime

class RawData:

    """
    this class imports the nested MATLAB files of the NASA Battery-Data repository:
    (https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/)
    into a nested cycle-based python-dict of the structure:
    {
    cycle1:
        ambient_temperature : value1
        operation_type : value1
        timestamp_of_cycleStart : value1
        Voltage_measured : [time_series_of_values]
        Current_measured : [time_series_of_values]
        Temperature_measured : [time_series_of_values]
        Current_charge : [time_series_of_values]
        Voltage_charge : [time_series_of_values]
        Time : [time_series_of_values]

    cycle2:
        ambient_temperature : value2
        operation_type : value2
        timestamp_of_cycleStart : value2
        Voltage_measured : [time_series_of_values]
        Current_measured : [time_series_of_values]
        Temperature_measured : [time_series_of_values]
        Current_load : [time_series_of_values]
        Voltage_load : [time_series_of_values]
        Time : [time_series_of_values]
        capacity : value2

    ...

    cycleN:
        ...
    }

    """

    def __init__(self, battery_path):
        # global variables:
        self.path = battery_path
        self.cycle = -1
        self.nested_raw_data_dict = {}

        # import functions:
        self.import_mat_file()
        self.iterate_through_levels(self.dataset)

    def import_mat_file(self):  # import .mat file and extraxted one (and only) data-array
        mat = sio.loadmat(self.path)
        for data in mat.keys():
            if (isinstance(mat[data], np.ndarray)):
                self.dataset = mat[data]

    def get_operation_type_at_cycle(self):
        return self.dataset['cycle'][0, 0]['type'][0, self.cycle][0]

    def get_ambient_temperature_at_cycle(self):  # if the temperature is not constant for the whole timeseries
        return self.dataset['cycle'][0, 0]['ambient_temperature'][0, self.cycle][0][0]

    def get_time_at_cycle(self):
        year = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][0])
        month = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][1])
        day = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][2])
        hour = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][3])
        minute = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][4])
        seconds = int(self.dataset['cycle'][0, 0]['time'][0, self.cycle][0][5])

        date = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=seconds)
        return date


    def iterate_through_levels(self, dataDimension):

        data = dataDimension
        list_of_elements = data.dtype.names

        if (list_of_elements != None):
            for element in list_of_elements:

                if (isinstance(data[element], np.ndarray)):

                    if (data[element].size <= 1):
                        self.iterate_through_levels(data[element][0, 0])

                        if element == "Voltage_measured" or element == "Sense_current": #first variable of cycle
                            self.cycle += 1
                            self.nested_raw_data_dict[self.cycle+1] = {}

                        self.nested_raw_data_dict[self.cycle+1]['ambient_temperature'] = self.get_ambient_temperature_at_cycle()
                        self.nested_raw_data_dict[self.cycle + 1]['operation_type'] = self.get_operation_type_at_cycle()
                        self.nested_raw_data_dict[self.cycle + 1]['timestamp_of_cycleStart'] = self.get_time_at_cycle()



                        if element != 'cycle':
                            value = data[element][0, 0][0]
                            if not len(value) < 1:
                                self.nested_raw_data_dict[self.cycle + 1][element] = value
                            else:
                                self.nested_raw_data_dict[self.cycle + 1][element] = [0] #if value is empty

                        if element == 'Time':
                            self.nested_raw_data_dict[self.cycle + 1]['length_of_cycleData'] = len(self.nested_raw_data_dict[self.cycle + 1]['Time'])

                        """shows a complex array entry of impedance:"""
                        # if element == 'Sense_current':
                        #    print(self.data[element][0, 0][0][0])




                    else:
                        for index in range(0, data[element].size):
                            self.iterate_through_levels(data[element][0, index])

    def print_dict(self):
        for cycle in self.nested_raw_data_dict:
            print(cycle)
            for variable_name in self.nested_raw_data_dict[cycle]:
                value = self.nested_raw_data_dict[cycle][variable_name]
                if isinstance(value, np.ndarray) and len(value) > 5:
                    print('  ' + variable_name, ':', value[0:2], ' ... ', value[-3:-1])
                else:
                    print('  ' + variable_name, ':', value)

if __name__ == "__main__":

    r = RawData('../../DataSets/5)Battery_Data_Set/5)BatteryAgingARC_49_50_51_52/B0050.mat')


