#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 17.06.20
Author: Tomy Hommel @TUD
"""

import numpy as np
import pandas as pd
from modules import filestorage, import_NASA_mat_file

class Battery:

    def __init__(self, battery_name):
        self.name = battery_name
        self.id = self.get_id()
        self.path = filestorage.Directory().get_file_path(self.name + '.mat')
        self.raw_data = import_NASA_mat_file.RawData(self.path).nested_raw_data_dict
        self.nominal_capacity = 2.01  # Nennkapazität in Ahr
        self.threshold_of_fade = 0.7
        self.reshape_rawData_to_chargeDischargeCycle()
        self.reshape_rawData_to_dischargeCycle_only()
        self.extract_battery_data()

        print(self.name + " is ready computed")

    def get_id(self):
        if self.name.startswith("B000"):
            return int(self.name[4:])
        else:
            return int(self.name[3:])

    def reshape_rawData_to_chargeDischargeCycle(self):
        """this function reshape the raw data, where a cycle is on of the operation type (charge, discharge, impedance)
        but most of the data-driven approaches see a charge+discharge combination as one cycle
        so a cycle is seen as a charge cycle compared with a discharge cycle and impedance test are ignored
        """
        self.charge_discharge_dict = {}
        charge_discharge_cycle = 1

        for cycle in range(1, len(self.raw_data)+1):

            i = cycle

            if self.raw_data[cycle]['operation_type'] == 'charge':
                charge_cycle = cycle

                while len(self.raw_data) >= i and self.raw_data[i]['operation_type'] != 'discharge':
                    i += 1

                    if len(self.raw_data) >= i and self.raw_data[i]['operation_type'] == 'charge':
                        #print('two times charge without a discharge in between at cycle: ' + str(charge_cycle) + ' and '+ str(i))

                        # in this chase the charge-cycle with more data is used:
                        if len(self.raw_data[charge_cycle]['Time']) < len(self.raw_data[i]['Time']):
                            charge_cycle = i

                if len(self.raw_data) >= i and self.raw_data[i]['operation_type'] == 'discharge':

                    self.charge_discharge_dict[charge_discharge_cycle] = {} #new charge-discharge-cycle
                    new = self.charge_discharge_dict[charge_discharge_cycle]
                    charge_discharge_cycle += 1

                    # charge data:
                    new['charge_ambient_temperature'] = self.raw_data[charge_cycle]['ambient_temperature']
                    new['charge_timestamp_of_cycleStart'] = self.raw_data[charge_cycle]['timestamp_of_cycleStart']
                    new['charge_voltage_measured'] = self.raw_data[charge_cycle]['Voltage_measured']
                    new['charge_current_measured'] = self.raw_data[charge_cycle]['Current_measured']
                    new['charge_temperature_measured'] = self.raw_data[charge_cycle]['Temperature_measured']
                    new['charge_current_charge'] = self.raw_data[charge_cycle]['Current_charge']
                    new['charge_voltage_charge'] = self.raw_data[charge_cycle]['Voltage_charge']
                    new['charge_time'] = self.raw_data[charge_cycle]['Time']
                    new['charge_length'] = self.raw_data[charge_cycle]['length_of_cycleData']

                    # discharge data:
                    new['discharge_ambient_temperature'] = self.raw_data[i]['ambient_temperature']
                    new['discharge_timestamp_of_cycleStart'] = self.raw_data[i]['timestamp_of_cycleStart']
                    new['discharge_voltage_measured'] = self.raw_data[i]['Voltage_measured']
                    new['discharge_current_measured'] = self.raw_data[i]['Current_measured']
                    new['discharge_temperature_measured'] = self.raw_data[i]['Temperature_measured']
                    new['discharge_current_load'] = self.raw_data[i]['Current_load']
                    new['discharge_voltage_load'] = self.raw_data[i]['Voltage_load']
                    new['discharge_time'] = self.raw_data[i]['Time']
                    new['discharge_capacity'] = self.raw_data[i]['Capacity'][0]
                    new['discharge_length'] = self.raw_data[i]['length_of_cycleData']

    def reshape_rawData_to_dischargeCycle_only(self):
        """this function reshape the raw data and only gets the discharge Cycles
        """
        self.discharge_dict = {}
        discharge_cycle = 1

        for cycle in range(1, len(self.raw_data)+1):

            if self.raw_data[cycle]['operation_type'] == 'discharge':

                self.discharge_dict[discharge_cycle] = {} #new discharge-cycle
                new = self.discharge_dict[discharge_cycle]

                # discharge data:
                new['discharge_ambient_temperature'] = self.raw_data[cycle]['ambient_temperature']
                new['discharge_timestamp_of_cycleStart'] = self.raw_data[cycle]['timestamp_of_cycleStart']
                new['discharge_voltage_measured'] = self.raw_data[cycle]['Voltage_measured']
                new['discharge_current_measured'] = self.raw_data[cycle]['Current_measured']
                new['discharge_temperature_measured'] = self.raw_data[cycle]['Temperature_measured']
                new['discharge_current_load'] = self.raw_data[cycle]['Current_load']
                new['discharge_voltage_load'] = self.raw_data[cycle]['Voltage_load']
                new['discharge_time'] = self.raw_data[cycle]['Time']
                new['discharge_capacity'] = self.raw_data[cycle]['Capacity'][0]
                new['discharge_length'] = self.raw_data[cycle]['length_of_cycleData']

                discharge_cycle += 1

    def set_threshold_of_fade(self, value):
        """function for setting up the threshold if not 70% by default
        threshold should by given as decimal value"""
        self.threshold_of_fade = value

    def extract_battery_data(self):
        self.discharge_capacity_array = self.extract_capacity()
        self.discharge_cycle_date_array = self.extract_values_of_feature('discharge_timestamp_of_cycleStart')
        self.all_cycles_total = len(self.raw_data)
        self.charge_discharge_cycles_total = len(self.charge_discharge_dict)
        self.eol = self.get_eol()   #n-ter Zuklus an dem die Kapazität den Schwellwert unterschreitet
        self.feature_list = self.extract_feature_list()
        self.plot_list = self.extract_plot_list()

    def extract_capacity(self):
        cap = []
        for cycle in range(1, len(self.charge_discharge_dict)+1):
            real_cap = self.charge_discharge_dict[cycle]['discharge_capacity']
            cap.append(real_cap)

        return cap

    def extract_feature_list(self):
        feature_list = []
        for cycle in self.charge_discharge_dict:
            for variable_name in self.charge_discharge_dict[cycle]:
                featurename = str(cycle)+'-'+variable_name
                feature_list.append(featurename)
        return feature_list

    def extract_plot_list(self):
        feature_list = []
        for cycle in self.charge_discharge_dict:
            for variable_name in self.charge_discharge_dict[cycle]:
                if not (variable_name == 'charge_ambient_temperature' or variable_name == 'charge_timestamp_of_cycleStart' \
                        or variable_name == 'charge_length' or variable_name == 'discharge_ambient_temperature' \
                        or variable_name == 'discharge_timestamp_of_cycleStart' or variable_name == 'discharge_length' \
                        or variable_name =='discharge_capacity'):
                    featurename = str(cycle)+'-'+variable_name
                    feature_list.append(featurename)
        return feature_list


    def extract_values_of_feature(self, feature):
        feature_array = []
        for cycle in range(1, len(self.charge_discharge_dict)+1):
            value = self.charge_discharge_dict[cycle][feature]
            feature_array.append(value)

        return feature_array

    def get_eol(self):

        for i in range(0, len(self.discharge_capacity_array)):

            if (self.discharge_capacity_array[i] / self.nominal_capacity <= self.threshold_of_fade):

                rul = list(reversed(range(0, i)))
                end = len(self.discharge_capacity_array) - len(rul)
                self.real_rul_timeseries = rul + list(reversed(range(-end, 0)))
                return i
        #if no rul can be calculated
        no_rul = list(range(1, len(self.discharge_capacity_array)+1))
        self.real_rul_timeseries = np.array(no_rul) * (-1)
        return -1


    def prepair_data_for_direct_approach(self):
        capacity = self.discharge_capacity_array
        d = {'cycle': np.arange(1, len(capacity) + 1).tolist(), 'capacity': capacity}
        return pd.DataFrame(data=d)


    def get_values_at_cycle(self, feature, cycle):
        """this function returns a array of a feature at a cycle - callable features:
        -charge_ambient_temperature
        -charge_timestamp_of_cycleStart
        -charge_voltage_measured
        -charge_current_measured
        -charge_temperature_measured
        -charge_current_charge
        -charge_voltage_charge
        -charge_time
        -charge_length

        -discharge_ambient_temperature
        -discharge_timestamp_of_cycleStart
        -discharge_voltage_measured
        -discharge_current_measured
        -discharge_temperature_measured
        -discharge_current_load
        -discharge_voltage_load
        -discharge_time
        -discharge_capacity
        -discharge_length
        """
        return self.charge_discharge_dict[cycle][feature]


    def print_dict(self):
        for cycle in self.charge_discharge_dict:
            for variable_name in self.charge_discharge_dict[cycle]:
                value = self.charge_discharge_dict[cycle][variable_name]
                if isinstance(value, np.ndarray) and len(value) > 5:
                    print('  ' + variable_name, ':', value[0:2], ' ... ', value[-3:-1])
                else:
                    print('  ' + variable_name, ':', value)


if __name__ == "__main__":
    b = Battery('B0005')
    b.print_dict()
    print(b.discharge_capacity_array[102])
    print(b.get_eol())
    print(len(b.real_rul_timeseries))
    print(b.real_rul_timeseries)