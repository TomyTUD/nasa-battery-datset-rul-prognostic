#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 30.06.20
Author: Tomy Hommel @TUD
"""
import os
import tkinter as tk
from tkinter import messagebox as mb, filedialog as fd
from modules import filestorage, DataSet
from modules.prognostic.preprocessing import plotter


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.background = '#4d4d4d'
        self.master = master
        self.pack()
        self.create_initial_size()
        self.create_widgets()

    def create_initial_size(self):
        #initial size:
        self.import_area = tk.Canvas(self, height=900, width=1400, bg='#333333')
        self.import_area.pack()

        self.frame = tk.Frame(self, bg=self.background)
        self.frame.place(relx=0.05, rely=0.05, relwidth=0.9,  relheight=0.9)


    def create_widgets(self):

        # Labels
        self.label_x = tk.Label(self.frame, text="Y-Achse:", bg=self.background, fg="White")
        self.label_x.place(relx=0.014, rely=0.15)

        self.label_y = tk.Label(self.frame, text="X-Achse:", bg=self.background, fg="White")
        self.label_y.place(relx=0.314, rely=0.15)

        self.label_info = tk.Label(self.frame, text="Battery Information:", bg=self.background, fg="White")
        self.label_info.place(relx=0.65, rely=0.15)

        self.infobox = tk.Text(self.frame, height=38, width=60, bg=self.background)
        self.infobox.place(relx=0.65, rely=0.2)


        # Listbox of features
        self.listbox = tk.Listbox(self.frame, selectmode='browse')
        self.listbox.place(relx=0.014, rely=0.2, width=400, height=500)
        self.listbox2 = tk.Listbox(self.frame, selectmode='browse')
        self.listbox2.place(relx=0.314, rely=0.2, width=400, height=500)

        # Scrollbar of Listboxes
        scrollbar = tk.Scrollbar(self.listbox2, orient="vertical", bg='#333333')
        self.listbox2.config(yscrollcommand=scrollbar.set)
        self.listbox2.config(yscrollcommand=scrollbar.set)


        scrollbar.config(command=self.yview)
        scrollbar.pack(side='right', fill='y')

        # Dropdown list of files
        paths, names, message = self.get_files()
        self.selected_variable = tk.StringVar(self)
        self.selected_variable.set('choose battery')
        self.selected_variable.trace("w", self.callback)

        self.list_of_files = tk.OptionMenu(self.frame, self.selected_variable, *names)
        self.list_of_files.config(bg=self.background)
        self.list_of_files.place(relx=0.01, rely=0.01)



        # Import Data Button
        self.import_data = tk.Button(self.frame, text="import data", command=self.get_data,
                                     highlightbackground=self.background)
        self.import_data.place(relx=0.011, rely=0.05, width=155)

        # Clear Button
        self.clear = tk.Button(self.frame, text="clear", command=self.clear_listbox, highlightbackground=self.background)
        self.clear.place(relx=0.15, rely=0.05, width=155)

        # Plot Button
        self.plot_button = tk.Button(self.frame, text="plot", command=self.plot_diagramm,
                               highlightbackground=self.background)
        self.plot_button.place(relx=0.38, rely=0.15, width=155)

        # Plot Capacity_timeless Button
        self.plot_capacity_timeless_button = tk.Button(self.frame, text="plot capacity timeless", command=self.plot_capacity_timeless,
                                      highlightbackground=self.background)
        self.plot_capacity_timeless_button.place(relx=0.51, rely=0.1, width=155)

        # Plot Capacity Button
        self.plot_capacity_button = tk.Button(self.frame, text="plot capacity", command=self.plot_capacity,
                                              highlightbackground=self.background)
        self.plot_capacity_button.place(relx=0.51, rely=0.15, width=155)

        # Quit Button
        self.quit = tk.Button(self.frame, text="QUIT", command=self.master.destroy, highlightbackground=self.background)
        self.quit.place(relx=0.95, rely=0.95)



    def yview(self, *args):
        """scroll function of both listboxes"""
        self.listbox.yview(*args)
        self.listbox2.yview(*args)

    def callback(self, *args):
        """callback function on changes of selected variable in the dropdown menu"""
        self.clear_listbox()
        self.get_data()

    def plot_diagramm(self):
        """plotts the choosen listbox element"""
        y_feature = self.listbox.get('active').split('-')
        x_feature = self.listbox2.get('active').split('-')

        y = self.battery.get_values_at_cycle(y_feature[1], int(y_feature[0]))
        x = self.battery.get_values_at_cycle(x_feature[1], int(x_feature[0]))

        featurename = y_feature[1]
        plotter.plot(featurename, x, y)

    def plot_capacity(self):
        x = self.battery.discharge_cycle_date_array
        y = self.battery.discharge_capacity_array

        featurename = 'capacity'
        plotter.plot(featurename, x, y, 1.4)

    def plot_capacity_timeless(self):
        y = self.battery.discharge_capacity_array
        x = list()

        for i in range(0, len(y)):
            x.append(i)

        featurename = 'capacity'
        plotter.plot(featurename, x, y, 1.4)

    def clear_listbox(self):
        """empty feature list and textbox from beginning to end"""
        self.listbox.delete(0, tk.END)
        self.listbox2.delete(0, tk.END)
        self.infobox.delete('1.0', tk.END)

    def get_data(self):
        """imports the data of selected variable / selected dataset """
        self.battery = DataSet.Battery(self.selected_variable.get())
        self.clear_listbox()

        for element in self.battery.plot_list:
            self.listbox.insert('end', element)
            self.listbox2.insert('end', element)

        self.infobox.insert(tk.END, '\nCharge-Discharge-Cycles Total: ' + str(self.battery.charge_discharge_cycles_total) + '\n')
        self.infobox.insert(tk.END, 'EOL-Cycle at Cycle: ' + str(self.battery.eol) + '\n')


    def get_files(self): #was passiert bei externen path?
        """inital setup of dropdown menu with the available battery data"""
        self.filestorage = filestorage.Directory()

        if not self.filestorage.check_dir():
            self.message('please choose the directory of your data storage in the following window')
            self.setup_filedirectory()

        return self.filestorage.list_files()

    def setup_filedirectory(self, dlgstr = 'Choose the directory of the output files.'):

        # defining options for opening a directory
        self.dir_opt = {}
        self.dir_opt['initialdir'] = os.curdir
        self.dir_opt['mustexist'] = True
        self.dir_opt['parent'] = self

        tmpdir = fd.askdirectory(title=dlgstr, **self.dir_opt)

        if tmpdir != '':
            self.filestorage.set_root_directory(tmpdir)
        if tmpdir == '':
            self.setup_filedirectory()

    def message(self, message):
            mb.showinfo('Info', message)


def start_gui():

    root = tk.Tk()
    root.title("NASA battery dataset")
    # set application to foreground:
    root.lift()
    root.attributes('-topmost', True)
    root.after_idle(root.attributes, '-topmost', False)

    # built gui:
    app = Application(master=root)
    app.mainloop()

if __name__ == "__main__":

    start_gui()


