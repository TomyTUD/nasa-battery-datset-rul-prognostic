#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 13.10.20 
Author: Tomy Hommel @TUD
"""
import numpy as np

def calculate_SOHs_based_on_rated_capacity(capacity):
    """
    the rated capacity of the batteries used in the NASA battery dataset is 2.0 Ahr
    :param capacity: single battery capacity array
    :return: soh as array
    """
    Co = 2.01
    soh = []
    for Ci in capacity:
        soh.append(Ci/Co)  #cycle capacity / rated capacity
    return np.array(soh)

def calculate_SOHs_based_on_initial_capacity(capacity):
    Co = capacity[0]
    soh = []
    for Ci in capacity:
        capacity.append(Ci/Co)  #current capacity / initial capacity
    return np.array(soh)