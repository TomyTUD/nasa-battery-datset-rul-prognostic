#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 15.10.20 
Author: Tomy Hommel @TUD
"""

from abc import ABC, abstractmethod
from scipy.stats import kurtosis, skew
import scipy
from scipy import integrate
from modules.DataSet import Battery
import pandas as pd
import numpy as np
import scipy.signal
import os
import tbd.outlier_detection as out
import tbd.interpolation as inter

class FeatureExtraction(ABC):
    def __init__(self):
        self.battery_list = []
        self.name = 0
        super().__init__()
        self.extracted_features = pd.DataFrame()


    def extract_class_features(self):
        for battery in self.battery_list:
            self.specific_metrics(battery)

    @abstractmethod
    def specific_metrics(self, battery):
        pass

    def save(self):
        self.extracted_features.to_csv(self.location, index=False)

    def check_if_csv_exist(self, externel_path=None):
        if externel_path != None:
            return os.path.isfile(externel_path)
        return os.path.isfile(self.location)


class FeatureExtraction_Partil2015(FeatureExtraction):

    def specific_metrics(self, battery):
        print(battery.id)




class FeatureExtraction_Partil2015(FeatureExtraction):

    def __init__(self):
        self.battery_list = 0
        self.name = 'Partil_2015'
        self.extracted_features = pd.DataFrame(columns=['id', 'cycle', 'minT', 'maxT', 'TminT', 'TmaxT', 'minV', 'maxV', 'TminV', 'TmaxV', 'Cap', 'TC_E', 'VC_E', 'TC_FI', 'VC_FI', 'TC_CI', 'VC_CI', 'TC_CCI', 'VC_CCI', 'TC_SI', 'VC_SI', 'TC_KI', 'VC_KI'])
        self.location = '../../../feature_tables/fe_partil2015.csv'
        self.x_label = ['minT', 'maxT', 'TminT', 'TmaxT', 'minV', 'maxV', 'TminV', 'TmaxV', 'Cap', 'TC_E', 'VC_E', 'TC_FI', 'VC_FI', 'TC_CI', 'VC_CI', 'TC_CCI', 'VC_CCI', 'TC_SI', 'VC_SI', 'TC_KI', 'VC_KI']

    def specific_metrics(self, battery):
        N = len(battery.charge_discharge_dict)
        ID = battery.id

        for cycle in range(1, N + 1):
            current = battery.charge_discharge_dict[cycle]['discharge_current_load']
            temperature = battery.charge_discharge_dict[cycle]['discharge_temperature_measured']
            voltage = battery.charge_discharge_dict[cycle]['discharge_voltage_measured']
            time = battery.charge_discharge_dict[cycle]['discharge_time']

            minT, maxT = self.min_max(temperature)
            TminT, TmaxT = self.min_max_at_time(time, temperature)
            minV, maxV = self.min_max(voltage)
            TminV, TmaxV = self.min_max_at_time(time, voltage)
            cap = self.capacity(time, current)
            TC_E = self.energy_of_temperature_signal(time, temperature)  # TC = temperature-curve
            VC_E = self.energy_of_voltage_signal(time, voltage)  # VC = voltage-curve
            TC_FI = self.fluctuation_index_of_signal(temperature, time)
            VC_FI = self.fluctuation_index_of_signal(voltage, time)
            TC_CI = self.curvature(time, temperature)
            VC_CI = self.curvature(time, voltage)
            TC_CCI = self.convex_concav_index(temperature)
            VC_CCI = self.convex_concav_index(voltage)
            TC_SI = self.skewness_index(temperature)
            VC_SI = self.skewness_index(voltage)
            TC_KI = self.kurtosis_index(temperature)
            VC_KI = self.kurtosis_index(voltage)

            row = {'id': int(ID), 'cycle': cycle, 'minT': minT, 'maxT': maxT, 'TminT': TminT, 'TmaxT': TmaxT, 'minV': minV, 'maxV': maxV, 'TminV': TminV, 'TmaxV': TmaxV, 'Cap': cap, 'TC_E': TC_E, 'VC_E': VC_E, 'TC_FI': TC_FI, 'VC_FI': VC_FI, 'TC_CI': TC_CI, 'VC_CI': VC_CI, 'TC_CCI': TC_CCI, 'VC_CCI': VC_CCI, 'TC_SI': TC_SI, 'VC_SI': VC_SI, 'TC_KI': TC_KI, 'VC_KI': VC_KI}
            self.extracted_features = self.extracted_features.append(row, ignore_index=True)


    def min_max(self, feature):
        return np.min(feature), np.max(feature)

    def min_max_at_time(self, time_series, feature):
        return time_series[np.argmin(feature)], time_series[np.argmax(feature)]

    def capacity(self, time_series, discharge_current):
        """this function calculates the capacity for a given current over a time period"""
        dx = 1  # dx entspricht Schrittweite bei Trapezbildung | defaultwert ist 1
        result = integrate.trapz(discharge_current, time_series, dx)
        return abs(result / 3600)  # Amperesekunden -> Amperestunden

    def derivative(self, x_data, y_data):
        N = len(x_data)
        delta_x = [x_data[i + 1] - x_data[i] for i in range(N - 1)]
        x_prim = [(x_data[i + 1] + x_data[i]) / 2. for i in range(N - 1)]
        y_prim = [(y_data[i + 1] - y_data[i]) / delta_x[i] for i in range(N - 1)]
        return x_prim, y_prim

    def curvature(self, x, y):
        N = len(x)
        x_1, y_1 = self.derivative(x, y)  # erste ableitung
        x_2, y_2 = self.derivative(x_1, y_1)  # zweite ableitung
        x_1, x_2 = self.resample_first_derivative(x_1, y_1)

        rho = []
        for n in range(0, len(x_1)):
            rho.append(y_2[n] / (1 + y_1[n] ** 2) ** (3 / 2))

        rho = rho[0], *rho, rho[-1]

        return sum(rho) / N

    def resample_first_derivative(self, x_data, y_data):
        N = len(x_data)
        x = [(x_data[i + 1] + x_data[i]) / 2. for i in range(N - 1)]
        y = [(y_data[i + 1] + y_data[i]) / 2. for i in range(N - 1)]
        return x, y

    def energy_of_temperature_signal(self, time_series, feature):  # Signalstärke
        """this function calculates the energy of a signal (voltage or temperature)"""

        # dx = time_series[-1]/len(time_series) #schrittweite bzw. abtastfrequenz
        dx = time_series[1] - time_series[0]
        # dx = 1/(time_series[-1]/len(time_series)) #samplefrequency
        f_welch, S_xx_welch = scipy.signal.welch(feature, fs=dx, nperseg=len(time_series))
        return sum([(abs(i) ** 2) for i in S_xx_welch])  # power density

    def energy_of_voltage_signal(self, time_series, feature):  # Signalstärke
        """this function calculates the energy of a signal (voltage or temperature)"""

        feature = abs(feature) ** 2
        dx = time_series[-1] / len(time_series)  # schrittweite bzw. abtastfrequenz
        result = integrate.trapz(feature, time_series, dx)
        result = result / 3600
        f_welch, S_xx_welch = scipy.signal.welch(feature, fs=dx, nperseg=len(time_series))
        return sum([(abs(i) ** 2) for i in S_xx_welch])  # power density


    def fluctuation_index_of_signal(self, feature, time_series):
        mean = np.mean(feature)
        sampling_frequency = 1 / (time_series[-1] / len(time_series))
        return sum([((i - mean) ** 2) for i in feature]) ** 0.5 / sampling_frequency

    def convex_concav_index(self, feature):
        """approach: https://mjo.osborne.economics.utoronto.ca/index.php/tutorial/index/1/cv1/t"""
        index = 0
        for x in range(1, len(feature) - 1):
            mean = (feature[x - 1] + feature[x + 1]) / 2
            if mean < feature[x]:  # convex
                index += 1
            if mean == feature[x]:
                pass
        return index / (len(feature) - 2)

    def skewness_index(self, feature):
        """https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.skew.html"""
        return skew(feature)

    def kurtosis_index(self, feature):
        """https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.kurtosis.html"""
        return kurtosis(feature, fisher=False)


class FeatureExtraction_Zhao2018(FeatureExtraction):

    def __init__(self):
        self.battery_list = 0
        self.name = 'Zhao_2018'
        self.extracted_features = pd.DataFrame(columns=['id', 'cycle', 'TIECVD', 'TIEDVD'])
        self.location = '../../../feature_tables/fe_zhao2018.csv'
        self.minV_dis = 3.6
        self.minV_cha = 3.8
        self.maxV_dis = 3.8
        self.maxV_cha = 4.2
        self.x_label = ['TIECVD', 'TIEDVD']


    def specific_metrics(self, battery):
        ID = battery.id

        tiecvd = self.extract_TIECVD(battery)
        tiedvd = self.extract_TIEDVD(battery)
        cycle = list(range(1, len(tiecvd)+1))

        for i in cycle:
            row = {'id': int(ID), 'cycle': cycle[i-1], 'TIECVD': tiecvd[i-1], 'TIEDVD': tiedvd[i-1]}
            self.extracted_features = self.extracted_features.append(row, ignore_index=True)



    def extract_TIECVD(self, battery):
        charge_time_dict = {}
        charge_voltage_dict = {}

        times_for_charge = []

        for key in battery.charge_discharge_dict.keys():
            """creates two dicts with the cycle as keys and an array for the measures as values"""
            charge_time_dict[key] = battery.charge_discharge_dict.get(key).get('charge_time')
            charge_voltage_dict[key] = battery.charge_discharge_dict.get(key).get('charge_voltage_measured')

        for cycle in charge_voltage_dict:
            """extracts the time of the specific voltage Interval"""
            measures = charge_voltage_dict.get(cycle)

            first_index = self.get_first_index_under_thresholt(measures, self.minV_cha)  # what happens when index None?

            # gets the voltage Interval of charge
            start_index = self.get_first_index_above_thresholt(measures, self.minV_cha, first_index=first_index + 1)
            ent_index = self.get_first_index_above_thresholt(measures, self.maxV_cha, first_index=first_index + 1)

            # print(measures)
            # print(cycle, start_index, ent_index)
            # print(measures[start_index-1], measures[start_index], measures[ent_index])


            # calculates the time Interval
            delta = charge_time_dict.get(cycle)[ent_index] - charge_time_dict.get(cycle)[start_index]

            if not isinstance(delta, np.float):
                delta = 0

            times_for_charge.append(delta)

        #print(times_for_charge)
        outliers = out.my_outlier_function(times_for_charge)
        times_for_charge = inter.interpolation(times_for_charge, outliers)
        outliers = out.find_zeros(times_for_charge)
        times_for_charge = inter.interpolation_gap(times_for_charge, outliers)

        return times_for_charge

    def extract_TIEDVD(self, battery):
        discharge_time_dict = {}
        discharge_voltage_dict = {}

        times_for_discharge = []

        for key in battery.charge_discharge_dict.keys():
            """creates two dicts with the cycle as keys and an array for the measures as values"""
            discharge_time_dict[key] = battery.charge_discharge_dict.get(key).get('discharge_time')
            discharge_voltage_dict[key] = battery.charge_discharge_dict.get(key).get('discharge_voltage_measured')

        for cycle in discharge_voltage_dict:
            """extracts the time of the specific voltage Interval"""
            measures = discharge_voltage_dict.get(cycle)

            first_index = self.get_first_index_above_thresholt(measures, self.maxV_dis)  # what happens when index None?

            if first_index == None:
                first_index = 0

            # gets the voltage Interval of discharge
            start_index = self.get_first_index_under_thresholt(measures, self.maxV_dis, first_index=first_index + 1)
            ent_index = self.get_first_index_under_thresholt(measures, self.minV_dis, first_index=first_index + 1)

            # calculates the time Interval
            times_for_discharge.append(
                discharge_time_dict.get(cycle)[ent_index] - discharge_time_dict.get(cycle)[start_index])

        return times_for_discharge

    def get_first_index_under_thresholt(self, measures, threshold, first_index = 0):
        for value in measures:
            if value <= threshold:
                return list(measures).index(value)
        return 0 #if None

    def get_first_index_above_thresholt(self, measures, threshold, first_index = 0):

        for value in measures:
            if value >= threshold and list(measures).index(value) >= first_index:
                return list(measures).index(value)


class FeatureExtraction_RealCapacity(FeatureExtraction):

    def __init__(self):
        self.battery_list = 0
        self.name = 'real_capacity'
        self.extracted_features = pd.DataFrame(columns=['id', 'cycle', 'real_cap'])
        self.location = '../../../feature_tables/fe_real_cap.csv'
        self.x_label = ['real_cap']


    def specific_metrics(self, battery):
        N = len(battery.charge_discharge_dict)
        ID = battery.id


        for cycle in range(1, N + 1):
            real_cap = battery.charge_discharge_dict[cycle]['discharge_capacity']
            row = {'id': int(ID), 'cycle': cycle, 'real_cap': real_cap}
            self.extracted_features = self.extracted_features.append(row, ignore_index=True)


class FeatureExtraction_All:

    def __init__(self):
        self.battery_list = 0
        self.name = 'all_features'
        self.extracted_features = pd.DataFrame(columns=['id', 'cycle'])
        self.location = '../../../feature_tables/fe_all.csv'


    def extract_class_features(self):
        fe1 = FeatureExtraction_Partil2015()
        fe1.battery_list = self.battery_list
        fe1.extract_class_features()
        df1 = fe1.extracted_features

        fe2 = FeatureExtraction_Zhao2018()
        fe2.battery_list = self.battery_list
        fe2.extract_class_features()
        df2 = fe2.extracted_features

        fe3 = FeatureExtraction_RealCapacity()
        fe3.battery_list = self.battery_list
        fe3.extract_class_features()
        df3 = fe3.extracted_features

        self.extracted_features = df1.merge(df2, on=['cycle', 'id'], how='inner')
        self.extracted_features = self.extracted_features.merge(df3, on=['cycle', 'id'], how='inner')

        self.save()

    def save(self):
        self.extracted_features.to_csv(self.location, index=False)

    def check_if_csv_exist(self, externel_path=None):
        if externel_path != None:
            return os.path.isfile(externel_path)
        return os.path.isfile(self.location)


if __name__ == '__main__':

    x = FeatureExtraction_Zhao2018()
    x.battery_list = [Battery('B0005')]
    x.extract_class_features()







