#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 23.07.20
Author: Tomy Hommel @TUD
"""
from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from modules import DataSet

class FeatureScaler(ABC):
    def __init__(self):
        self.origin_data = []
        self.scaled_data = 0
        self.scaler_dict = 0
        self.new_data = 0
        self.name = 0
        super().__init__()

    @abstractmethod
    def scale(self, df=None):
        """this function fits the train data"""
        pass

    @abstractmethod
    def compute(self, test_df=None):
        """this function applys the train-statistics on the test_data"""
        pass

    @abstractmethod
    def rescale_single(self, column_name, value):
        pass

class FeatureScaler_MinMax(FeatureScaler):
    def __init__(self):
        self.name = 'MinMax_scaler'
        self.scaler_dict = 0
        self.x_label = []


    def scale(self, df = None, ignore_columns = None):
        """
        this function accepts a pandas DataFrame and scales all the data by column to range: [0,1]

        :param data: DataFrame
        :return: array [DataFrame, list] of [scaled_date, scaler_objects]
        """

        constant_df = None

        if df is not None:
            self.origin_data = df.reset_index(drop=True)

        if ignore_columns is not None:
            constant_df = self.origin_data[ignore_columns]
            df = self.origin_data.drop(columns=ignore_columns)

        columnames = df.columns

        d = df.values
        scaler = []
        new_data = []
        new_column_names = []

        for column in range(d.shape[1]):
            mms_column =  MinMaxScaler()
            column_data_scaled = mms_column.fit_transform(d[:, column].reshape(-1, 1))
            new_data.append(column_data_scaled.ravel())
            new_column_names.append(columnames[column] + '_scaled')
            scaler.append(mms_column)  # save individual scaler

        for i in range(len(self.x_label)):
            if not self.x_label[i].endswith('_scaled'):
                self.x_label[i] = self.x_label[i]+'_scaled'

        data = pd.DataFrame(data=np.array(new_data).transpose(), columns=new_column_names)


        scaler_dict = {}
        for column in range(len(new_column_names)):
            scaler_dict[new_column_names[column]] = scaler[column]

        self.scaled_data = data
        self.scaler_dict = scaler_dict

        if constant_df is not None:
            self.new_data = pd.concat([constant_df.iloc[:, :1], self.scaled_data], axis=1)
            l = constant_df.shape[1]-1
            self.new_data = pd.concat([self.new_data, constant_df.iloc[:, -l:]], axis=1)
        else:
            self.new_data = self.scaled_data

        return self.new_data

    def compute(self, test_df=None):
        """this function applys the train-statistics on the test_data"""

        for column in self.scaler_dict.keys():

            scaled_column = self.scaler_dict[column].transform(np.array(test_df[column[:-7]]).reshape(-1, 1))
            test_df = test_df.rename(columns={column[:-7]: column})
            test_df[column] = scaled_column

        return test_df

    def rescale_single(self, column_name, value):
        return self.scaler_dict[column_name].inverse_transform(np.array(value).reshape(-1, 1))[0][0]

    def rescale_series(self, column_name, values):
        return self.scaler_dict[column_name].inverse_transform(np.array(values).reshape(-1, 1))



class FeatureScaler_Standard(FeatureScaler):
    def __init__(self):
        self.name = 'Standard_scaler'
        self.scaler_dict = 0
        self.x_label = []

    def scale(self, df = None, ignore_columns = None):
        """
        this function accepts a pandas DataFrame and scales/standardize all the data by column around zero (mean)

        :param data: DataFrame
        :return: array [DataFrame, list] of [scaled_date, scaler_objects]
        """
        constant_df = None

        if df is not None:
            self.origin_data = df.reset_index(drop=True)

        if ignore_columns is not None:
            constant_df = self.origin_data[ignore_columns]
            df = self.origin_data.drop(columns=ignore_columns)

        columnames = df.columns

        d = df.values
        scaler = []
        new_data = []
        new_column_names = []

        for column in range(d.shape[1]):
            sc_column = StandardScaler()
            column_data_scaled = sc_column.fit_transform(d[:, column].reshape(-1, 1))
            new_data.append(column_data_scaled.ravel())
            new_column_names.append(columnames[column] + '_scaled')
            scaler.append(sc_column)  # save individual scaler

        data = pd.DataFrame(data=np.array(new_data).transpose(), columns=new_column_names)

        for i in range(len(self.x_label)):
            if not self.x_label[i].endswith('_scaled'):
                self.x_label[i] = self.x_label[i] + '_scaled'

        scaler_dict = {}
        for column in range(len(new_column_names)):
            scaler_dict[new_column_names[column]] = scaler[column]

        self.scaled_data = data
        self.scaler_dict = scaler_dict

        if constant_df is not None:
            self.new_data = pd.concat([constant_df.iloc[:, :1], self.scaled_data], axis=1)
            l = constant_df.shape[1] - 1
            self.new_data = pd.concat([self.new_data, constant_df.iloc[:, -l:]], axis=1)

        else:
            self.new_data = self.scaled_data

        return self.new_data

    def compute(self, test_df=None):
        """this function applys the train-statistics on the test_data"""

        for column in self.scaler_dict.keys():
            scaled_column = self.scaler_dict[column].transform(np.array(test_df[column[:-7]]).reshape(-1, 1))
            test_df = test_df.rename(columns={column[:-7]: column})
            test_df[column] = scaled_column

        return test_df

    def rescale_single(self, column_name, value):
        return self.scaler_dict[column_name].inverse_transform(np.array(value).reshape(-1, 1))[0][0]

    def rescale_series(self, column_name, values):
        return self.scaler_dict[column_name].inverse_transform(np.array(values).reshape(-1, 1))


class FeatureScaler_PCA(FeatureScaler):
    def __init__(self):
        self.name = 'MinMax_scaler'
        self.scaler_dict = 0
        self.x_label = []


    def scale(self, df = None, ignore_columns = None):
        """
        this function accepts a pandas DataFrame and scales all the data by column to range: [0,1]

        :param data: DataFrame
        :return: array [DataFrame, list] of [scaled_date, scaler_objects]
        """

        constant_df = None

        if df is not None:
            self.origin_data = df.reset_index(drop=True)

        if ignore_columns is not None:
            constant_df = self.origin_data[ignore_columns]
            df = self.origin_data.drop(columns=ignore_columns)

        columnames = df.columns


        d = df.values
        d = d*-1

        scaler = []
        new_data = []
        new_column_names = []

        for column in range(d.shape[1]):
            mms_column =  MinMaxScaler()
            column_data_scaled = mms_column.fit_transform(d[:, column].reshape(-1, 1))
            new_data.append(column_data_scaled.ravel())
            new_column_names.append(columnames[column] + '_scaled')
            scaler.append(mms_column)  # save individual scaler

        for i in range(len(self.x_label)):
            if not self.x_label[i].endswith('_scaled'):
                self.x_label[i] = self.x_label[i]+'_scaled'

        data = pd.DataFrame(data=np.array(new_data).transpose(), columns=new_column_names)


        scaler_dict = {}
        for column in range(len(new_column_names)):
            scaler_dict[new_column_names[column]] = scaler[column]

        self.scaled_data = data
        self.scaler_dict = scaler_dict

        if constant_df is not None:
            self.new_data = pd.concat([constant_df.iloc[:, :1], self.scaled_data], axis=1)
            l = constant_df.shape[1]-1
            self.new_data = pd.concat([self.new_data, constant_df.iloc[:, -l:]], axis=1)
        else:
            self.new_data = self.scaled_data

        return self.new_data

    def compute(self, test_df=None):
        """this function applys the train-statistics on the test_data"""

        for column in self.scaler_dict.keys():

            column_values = test_df[column[:-7]] * -1
            scaled_column = self.scaler_dict[column].transform(np.array(column_values).reshape(-1, 1))
            test_df = test_df.rename(columns={column[:-7]: column})
            test_df[column] = scaled_column

        return test_df

    def rescale_single(self, column_name, value):
        return self.scaler_dict[column_name].inverse_transform(np.array(value).reshape(-1, 1))[0][0]

    def rescale_series(self, column_name, values):
        return self.scaler_dict[column_name].inverse_transform(np.array(values).reshape(-1, 1))

class FeatureScaler_do_nothing(FeatureScaler):
    """
    this class only renames the data-header to keep it consistent
    """

    def __init__(self):
        self.name = 'nothing_scaled'
        self.scaler_dict = 0

    def scale(self, df=None, ignore_columns = None):

        if ignore_columns is not None:
            constant_df = self.origin_data[ignore_columns]
            df = self.origin_data.drop(columns=ignore_columns)

        columnames = df.columns
        new_column_names = []

        for column in columnames:
            df.rename(columns={column: column+'_scaled'}, inplace=True)
            new_column_names.append(column + '_scaled')

        scaler_dict = {}
        for column in range(len(new_column_names)):
            scaler_dict[new_column_names[column]] = 0


        self.scaler_dict = scaler_dict

        self.scaled_data = df

        if constant_df is not None:
            self.new_data = pd.concat([constant_df.iloc[:, :1], self.scaled_data], axis=1)
            l = constant_df.shape[1] - 1
            self.new_data = pd.concat([self.new_data, constant_df.iloc[:, -l:]], axis=1)

        return self.new_data

    def compute(self, test_df=None):

        for column in self.scaler_dict.keys():
            test_df = test_df.rename(columns={column[:-7]: column})

        return test_df

    def rescale_single(self, column_name, value):
        return value

    def rescale_series(self, column_name, values):
        return np.array(values).reshape(-1, 1)


