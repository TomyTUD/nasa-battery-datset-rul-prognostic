#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 23.10.20 
Author: Tomy Hommel @TUD
"""
from PyEMD import Visualisation, EMD
import numpy as np
from modules.DataSet import Battery
import pandas as pd


class my_EMD():
    def __init__(self):
        self.name = 'EMD'
        self.input_data = 0
        self.constant_df = None
        self.ignore_columns = []
        self.df_for_emd = None
        self.calculated_data = 0
        self.model = 0
        self.imfs = 0
        self.res = 0
        self.t = 0

    def import_data(self, data):
        self.input_data = data

    def extract_IMFS(self, plot=False):
        """Extract imfs and residue"""

        battery_list = []
        battery_list.append(self.input_data)

        for i in battery_list:
            ts = i.iloc[:, 0]
            self.t = i.iloc[:, 1]
            emd = EMD(nbsym=3)
            emd.total_power_thr = 0.05
            emd.emd(np.array(ts))
            self.imfs, self.res = emd.get_imfs_and_residue()

            if plot:
                self._plot_extracted_IMFS()


    def _plot_extracted_IMFS(self):
        vis = Visualisation()
        vis.plot_imfs(imfs=self.imfs[:-1], residue=self.imfs[-1], t=self.t, include_residue=True)
        vis.show()



if __name__ == '__main__':
    cap = Battery('B0005').discharge_capacity_array
    cycles = list(range(len(cap)))
    d = {'cap': cap, 'cycle': cycles}
    df = pd.DataFrame(d)

    decomp = my_EMD()
    decomp.import_data(df)
    decomp.extract_IMFS(plot=True)