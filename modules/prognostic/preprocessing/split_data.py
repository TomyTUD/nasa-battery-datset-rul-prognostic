#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 29.09.20 
Author: Tomy Hommel @TUD
"""

import numpy as np


def split_to_train_and_test(data, split_index=100):
    """
    this function split the data into the train and test set for x and y values if you want to split the data at a specific cycle -> default: 100 (split_index)

    :param data: array of single column
    :return: scaled x and y set of train and test data
    """
    x = np.array(range(len(data))) + 1

    y_train = data[:split_index]
    y_test = data[split_index:]
    x_train = x[:split_index] #np.array(range(len(y_train))) + 1
    x_test = x[split_index:] #np.array(range(len(y_test))) + x_train[-1]
    return x_train, y_train, x_test, y_test