#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 16.10.20 
Author: Tomy Hommel @TUD
"""
import pandas as pd
from modules.DataSet import Battery
from modules.prognostic.preprocessing import calculateSOH as cSOH


def label_data(data: pd.DataFrame, battery_list):
    batterys = data['id'].unique()
    rul_label = []
    soh_label = []

    for name in battery_list:
        b = Battery(name)
        soh_label = soh_label + list(cSOH.calculate_SOHs_based_on_rated_capacity(b.discharge_capacity_array))
        rul_label = rul_label + list(b.real_rul_timeseries)

    data['rul_label'] = rul_label
    data['soh_label'] = soh_label

    #for classification:
    data['fail_within_10_cycles'] = 0
    data.loc[data['rul_label'] <= 10, 'fail_within_10_cycles'] = 1

    return data