#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 17.07.20
Author: Tomy Hommel @TUD
"""
from modules import DataSet
import numpy as np

def scale(set1, set2): #for slope
    """function computes the change in percent from on measurement 'm' to the next measurement 'm+1'
    Example: ['m'=1, 'm+1'=2] -> result = 1 for rise of 100% """

    return np.diff(set1), np.diff(set2)

def correlation(*args):
    """output of numpy correlation looks like:
    [[a, b]
     [c, d]]
    where a is the correlation of set1 to itself, same for d (set2)
    b and c is the correlation between set1 and set2"""
    return np.corrcoef(list(args))

def correlation_at_cycle(battery, cycle):
    """this function calculates the correlation of all time_series featuers within a charge-discharge-cycle of a specific battery"""

    charge = list()
    discharge = list()

    charge.append(battery.get_values_at_cycle('charge_voltage_measured', cycle))
    charge.append(battery.get_values_at_cycle('charge_current_measured', cycle))
    charge.append(battery.get_values_at_cycle('charge_temperature_measured', cycle))
    charge.append(battery.get_values_at_cycle('charge_current_charge', cycle))
    charge.append(battery.get_values_at_cycle('charge_voltage_charge', cycle))
    charge.append(battery.get_values_at_cycle('charge_time', cycle))

    discharge.append(battery.get_values_at_cycle('discharge_voltage_measured', cycle))
    discharge.append(battery.get_values_at_cycle('discharge_current_measured', cycle))
    discharge.append(battery.get_values_at_cycle('discharge_temperature_measured', cycle))
    discharge.append(battery.get_values_at_cycle('discharge_current_load', cycle))
    discharge.append(battery.get_values_at_cycle('discharge_voltage_load', cycle))
    discharge.append(battery.get_values_at_cycle('discharge_time', cycle))

    charge_correlations = np.corrcoef(charge)
    discharge_correlations = np.corrcoef(discharge)

    return charge_correlations, discharge_correlations

if __name__ == "__main__":

    b36 = DataSet.Battery('B0036')

    set1 = b36.get_values_at_cycle('discharge_voltage_measured', 130)
    set2 = b36.get_values_at_cycle('discharge_voltage_load', 130)

    print(correlation_at_cycle(b36, 99))

