#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 15.10.20 
Author: Tomy Hommel @TUD
"""
import random
import pandas as pd
from modules import filestorage
from modules.DataSet import Battery

class DataSelection_all:

    def __init__(self):
        self.list_of_batteries = 0
        self.name = 'all_batterys'
        self.info = 'all provided batteries'
        self.battery_names = self.get_names()

    def get_names(self):
        paths, names, message = filestorage.Directory().list_files()
        return names

    def import_data(self):
        """this function provides a list of all batteries of a given root directory"""
        names = self.battery_names
        list_of_batteries = []
        for battery_name in names:
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

class DataSelection_multiple:

    def __init__(self):
        self.list_of_batteries = 0
        self.name = 'multiple_battery_profile'
        self.info = 'all batteries with working condition from 24-44 degree celsius'
        self.battery_names = ['B0005', 'B0006', 'B0007', 'B0018', 'B0025', 'B0026', 'B0027', 'B0028', 'B0029', 'B0030', 'B0031', 'B0032', 'B0033', 'B0034', 'B0036', 'B0038', 'B0039', 'B0040']

    def import_data(self):
        """this function only provide the following batteries: (B05, B06, B07, B18, B25, B26, B27, B28, B29, B30, B31, B32, B33, B34, B36, B38, B39, B40)"""
        names = ['B0005', 'B0006', 'B0007', 'B0018', 'B0025', 'B0026', 'B0027', 'B0028', 'B0029', 'B0030', 'B0031', 'B0032', 'B0033', 'B0034', 'B0036', 'B0038', 'B0039', 'B0040']
        list_of_batteries = []
        for battery_name in names:
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

class DataSelection_single:

    def __init__(self):
        self.list_of_batteries = 0
        self.name = 'single_battery_profile'
        self.info = 'batteries for rul prognostic'
        self.battery_names = ['B0005', 'B0006', 'B0007', 'B0018']

    def import_data(self):
        """this function only provide the following batteries: (B05, B06, B07, B18)"""
        names = self.battery_names
        list_of_batteries = []
        for battery_name in names:
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

class DataSelection_specific:

    def __init__(self, battery_list):
        self.list_of_batteries = 0
        self.name = 'specific_batterys'
        self.info = 'batteries for rul prognostic'
        self.battery_names = battery_list

    def import_data(self):
        """this function only provide the following batteries: (B05, B06, B07, B18)"""
        names = self.battery_names
        list_of_batteries = []
        for battery_name in names:
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

class DataSelection_no_rul:

    def __init__(self):
        self.list_of_batteries = 0
        self.name = 'no_rul_battery'
        self.info = 'batteries which never reach eol'
        self.battery_names = ['B0025', 'B0027', 'B0028', 'B0029', 'B0030', 'B0031', 'B0032']

    def import_data(self):
        """this function only provide the following batteries: (B25, B27, B28, B29, B30, B31, B32)"""
        names = self.battery_names
        list_of_batteries = []
        for battery_name in names:
            print(battery_name)
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

class DataSelection_4degreeCelsius:

    def __init__(self):
        self.list_of_batteries = 0
        self.name = '4degreeCelsius_battery_profile'
        self.info = 'all batteries with working condition of 4 degree celsius'
        self.battery_names = ['B0041', 'B0042', 'B0043', 'B0044', 'B0045', 'B0046', 'B0047', 'B0048', 'B0049', 'B0050', 'B0051', 'B0052', 'B0053', 'B0054', 'B0055', 'B0056']

    def import_data(self):
        names = self.battery_names
        list_of_batteries = []
        for battery_name in names:
            list_of_batteries.append(Battery(battery_name))
        self.list_of_batteries = list_of_batteries
        return list_of_batteries

def get_df(battery_data_selection, selected_features):

    split = selected_features.location.split('/')
    loc = '../../feature_tables/' + battery_data_selection.name + '_' + split[-1]


    if not selected_features.check_if_csv_exist(externel_path=loc):  # check if csv exist

        battery_list = battery_data_selection.import_data()
        selected_features.battery_list = battery_list

        print('extract features from raw data')

        selected_features.location = loc  # setup the relativ path
        selected_features.extract_class_features()
        selected_features.save()
        data = selected_features.extracted_features

    else:
        print('features allready exist')
        data = pd.read_csv(loc)

    return data

def get_df_for_test(battery_data_selection, selected_features):

    battery_list = battery_data_selection.import_data()
    selected_features.battery_list = battery_list
    selected_features.extract_class_features()
    data = selected_features.extracted_features
    return data



def split_data(data: pd.DataFrame, random_seed=42):
    batterys = data['id'].unique()
    l = int(len(batterys)/2) #abgerundet

    random.seed(random_seed)
    random.shuffle(batterys)

    train = batterys[l:]
    test = batterys[:l]

    train_data = pd.DataFrame()
    test_data = pd.DataFrame()

    for i in train:
        sub = data[data['id'] == i]
        train_data = pd.concat([train_data, sub])
        train_data.sort_index(inplace=True)

    for i in test:
        sub = data[data['id'] == i]
        test_data = pd.concat([test_data, sub])
        test_data.sort_index(inplace=True)

    return train_data, test_data

if __name__ == '__main__':
     ds_all =DataSelection_single()
     ds_all.import_data()
     print(ds_all.list_of_batteries) #list of single_profile_batterys