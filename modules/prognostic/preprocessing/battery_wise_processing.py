#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 20.10.20
Author: Tomy Hommel @TUD
"""
import pandas as pd


def extract_X_and_Y_batterywise(df: pd.DataFrame, x_label=['soh_label_scaled'], y_label=['cycle_scaled']):
    battery_data_list = []

    for id in df['id'].unique():
        battery_data_list.append(df[df['id'] == id])

    for i in range(len(battery_data_list)):
        battery_data_list[i] = battery_data_list[i][x_label + y_label]

    return battery_data_list

def prepair_batterywise_plot(df1: pd.DataFrame, df2: pd.DataFrame):

    return pd.merge(df1['id'], df2, left_index=True, right_index=True)

def split_by_battery(df: pd.DataFrame):
    battery_data_list = []

    for id in df['id'].unique():
        battery_data_list.append(df[df['id'] == id])

    return battery_data_list

def index_by_battery(df: pd.DataFrame):
    battery_index_list = []

    for id in df['id'].unique():
        battery_index_list.append(df[df['id'] == id].index)

    return battery_index_list

def split_by_cycle(battery_list: list, split_cycle):
    train_data_list = []
    test_data_list = []

    for battery_df in battery_list:
        for id in battery_df['id'].unique():
            scope = battery_df[battery_df['id'] == id]
            train_data_list.append(scope.iloc[:split_cycle, :])
            test_data_list.append(scope.iloc[split_cycle:, :])

    return train_data_list, test_data_list


def extract_battery_id(battery_list: list):
    id_list = []

    for battery_df in battery_list:
        for id in battery_df['id'].unique():
            id_list.append(id)

    return id_list