#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 18.10.20 
Author: Tomy Hommel @TUD
"""
from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA as PCA_learn

class DimRed(ABC):
    def __init__(self):
        self.name = ''
        self.input_data = 0
        self.constant_df = None
        self.ignore_columns = []
        self.df_for_pca = None
        self.calculated_data = 0
        self.model = 0
        super().__init__()

    @abstractmethod
    def import_data(self, ignore_columns = None, data = None):
        pass

    @abstractmethod
    def fit(self):
        pass

    @abstractmethod
    def compute(self):
        pass


class PCA(DimRed):
    """
    PCA-Tutorial
    https://www.codementor.io/@divyeshaegis/when-to-use-pca-before-or-after-a-train-test-split-vxdrlu6ci
    """

    def __init__(self):
        self.name = 'PCA'
        self.eigen = 0
        self.input_data = 0
        self.constant_df = None
        self.ignore_columns = []
        self.df_for_pca = None
        self.calculated_data = 0
        self.model = 0
        self.x_label = []

    def import_data(self, ignore_columns = None, data = None):
        """
        this function takes a dataframe and ignores the list of columns which is passt
        the dataframe should provide n columns -> first n-1 columns as inputs and last column as target-column

        :param ignore: list of columnnames which should be ignored
        :return: prepaired df for pca computing
        """
        if data is not None:
            self.input_data = data

        if ignore_columns is not None:
            self.constant_df = self.input_data
            self.ignore_columns = ignore_columns

            self.df_for_pca = self.input_data.drop(columns=ignore_columns)
        else:

            self.df_for_pca = self.input_data


    def fit(self):
        """keep components until a overall variance of at least 90% is explained"""
        explained_variance = 0.90

        self.model = PCA_learn(explained_variance)
        self.model.fit(X=self.df_for_pca)
        computed_data = self.model.transform(self.df_for_pca)

        self.x_label = []
        self.calculated_data = self.constant_df.iloc[:, :0]

        for i in range(computed_data.shape[1]):
            label = 'pca_' + str(i)
            self.x_label.append(label)
            self.calculated_data[label] = computed_data[:, i]

        self.calculated_data = pd.concat([self.calculated_data, self.constant_df], axis=1)

        return self.calculated_data


    def compute(self, test_data):
        origin_test_df = test_data.copy()
        df = test_data.drop(columns=self.ignore_columns)
        computed_df = self.model.transform(df)

        return_test_df = origin_test_df.iloc[:, :0]

        for i in range(computed_df.shape[1]):
            label = 'pca_' + str(i)
            return_test_df[label] = computed_df[:, i]

        return_test_df = pd.concat([return_test_df, origin_test_df], axis=1)

        return return_test_df

    def show_model_details(self):
        print(np.round(self.model.explained_variance_ratio_, 3))
        print(self.model.explained_variance_)
        self.plot_explained_variance()


    def plot_explained_variance(self):

        plt.figure()
        plt.plot(np.cumsum(self.model.explained_variance_ratio_))
        plt.xlabel('Number of Components')
        plt.ylabel('Variance (%)')
        plt.title('Explained Variance')
        plt.show()

class PCA_010(PCA):
    def __init__(self):
        super().__init__()
        self.name = 'PCA_010'

    def fit(self):
        """only keep components which explain a variance of at least 10%"""
        explained_ratio = 0.1

        self.model = PCA_learn()
        self.model.fit(X=self.df_for_pca)

        keep_n_components = np.count_nonzero(self.model.explained_variance_ratio_ > explained_ratio)
        #print(keep_n_components)

        self.model = PCA_learn(n_components=keep_n_components)
        self.model.fit(X=self.df_for_pca)
        computed_data = self.model.transform(self.df_for_pca)

        self.calculated_data = self.constant_df.iloc[:, :2]

        for i in range(computed_data.shape[1]):
            label = 'pca_' + str(i)
            self.calculated_data[label] = computed_data[:, i]

        self.calculated_data = pd.concat([self.calculated_data, self.constant_df.iloc[:, 2:]], axis=1)

        return self.calculated_data

class manual_selection:
    def __init__(self, feature):
        if feature == 'TIECVD_scaled':
            self.name = 'manual_selection'+'_'+'TIEDVD_scaled'
        elif feature == 'TIEDVD_scaled':
            self.name = 'manual_selection'+'_'+'TIECVD_scaled'
        else:
            self.name = 'manual_selection_both'
        self.input_data = 0
        self.constant_df = None
        self.ignore_columns = []
        self.df_for_pca = None
        self.calculated_data = 0
        self.model = 0
        self.x_label = []
        self.feature = feature


    def import_data(self, ignore_columns = None, data = None):
        if data is not None:
            self.input_data = data
            self.calculated_data = data

    def fit(self):
        if self.feature == 'TIECVD_scaled':
            self.x_label = ['TIEDVD_scaled']
            return self.calculated_data.drop([self.feature], axis=1)
        elif self.feature == 'TIEDVD_scaled':
            self.x_label = ['TIECVD_scaled']
            return self.calculated_data.drop([self.feature], axis=1)

        else:
            self.x_label = ['both']
            self.calculated_data['both'] = self.calculated_data[['TIECVD_scaled', 'TIEDVD_scaled']].mean(axis=1)
            self.calculated_data.drop(['TIECVD_scaled'], axis=1, inplace=True)
            self.calculated_data.drop(['TIEDVD_scaled'], axis=1, inplace=True)
            return self.calculated_data


    def compute(self, test_data):
        if self.feature == 'both':
            test_data['both'] = test_data[['TIECVD_scaled', 'TIEDVD_scaled']].mean(axis=1)
            test_data.drop(['TIECVD_scaled'], axis=1, inplace=True)
            test_data.drop(['TIEDVD_scaled'], axis=1, inplace=True)
            return test_data
        else:
            return test_data.drop([self.feature], axis=1)

class do_nothing(DimRed):
    def __init__(self):
        self.name = 'no_dim-reduction'
        self.input_data = 0
        self.constant_df = None
        self.ignore_columns = []
        self.df_for_pca = None
        self.calculated_data = 0
        self.model = 0
        self.x_label = []


    def import_data(self, ignore_columns = None, data = None):
        if data is not None:
            self.input_data = data
            self.calculated_data = data

    def fit(self):
        return self.calculated_data

    def compute(self, test_data):
        return test_data
