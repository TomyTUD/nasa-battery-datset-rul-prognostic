#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 24.06.20
Author: Tomy Hommel @TUD
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def scatter(x, y):
    plt.scatter(x, y)
    plt.show()

def scatter_single(x):
    y = []
    for i in range(len(x)):
        y.append(i)

    plt.scatter(y, x)
    plt.show()

def plot(featurename, x=None, y=None, threshold=None):

    fig = plt.figure(featurename+' timeseries')
    plt.plot(x, y, markersize=1)
    plt.legend([featurename])

    if threshold:
        plt.axhline(y=threshold)
        plt.legend([featurename, 'threshold'])

    plt.show()


def plot_decision_boundary(X, y, clf):

    X_set, y_set = X, y
    X1, X2 = np.meshgrid(np.arange(start=X_set[:, 0].min() - 1,
                                   stop=X_set[:, 0].max() + 1,
                                   step=0.01),
                         np.arange(start=X_set[:, 1].min() - 1,
                                   stop=X_set[:, 1].max() + 1,
                                   step=0.01))

    print(X1)
    print(X2)

    Z = clf.predict(np.c_[X1.ravel(), X2.ravel()])
    Z = Z.reshape(X1.shape)
    plt.pcolormesh(X1, X2, Z, cmap=plt.cm.Paired)

    for i, element in enumerate(y):
       if element == 'A':
           y[i] = 1
       if element == 'B':
           y[i] = 2
       if element == 'C':
           y[i] = 3
       if element == 'D':
           y[i] = 4

    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Paired, edgecolors='k')
    plt.title('3-Class classification using Support Vector Machine with custom'
              ' kernel')
    plt.axis('tight')
    return plt.show()

    #plt.contourf(X1, X2, clf.predict(np.array([X1.ravel(),
    #                                           X2.ravel()]).T).reshape(X1.shape), alpha=0.75, cmap=zero_one_colourmap)
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c=(zero_one_colourmap)(i), label=j)
    plt.title('SVM Decision Boundary')
    plt.xlabel('X1')
    plt.ylabel('X2')
    plt.legend()
    return plt.show()

def plot_error_percentage(error, ci_stats):
    count = range(1, len(error)+1)

    if len(ci_stats) != 0:
       mean, LB, UB = ci_stats
       plt.axhline(y=LB, linestyle='--')
       plt.axhline(y=UB, linestyle='--')

    plt.scatter(count, error)
    plt.show()

def plot_true_vs_predicted(true, pred):
    count = range(1, len(true) + 1)
    plt.plot(count, true, linestyle='-', marker='o', color='black')
    plt.plot(count, pred, linestyle='-', marker='o', color='red')
    plt.show()

def plot_classification(X, y, clf):
    for i, element in enumerate(y):
       if element == 'A':
           y[i] = 1
       if element == 'B':
           y[i] = 2
       if element == 'C':
           y[i] = 3
       if element == 'D':
           y[i] = 4


    h = .02  # step size in the mesh
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

    for i, element in enumerate(Z):
       if element == 'A':
           Z[i] = 1
       if element == 'B':
           Z[i] = 2
       if element == 'C':
           Z[i] = 3
       if element == 'D':
           Z[i] = 4

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)

    # Plot also the training points
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
    plt.xlabel('Sepal length')
    plt.ylabel('Sepal width')
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks(())
    plt.yticks(())
    plt.title('SVM')

    plt.show()


def plot_multiple_batteriedata_1D(df, x_label, y_label):
    ids = df['battery'].unique()
    fig, axs = plt.subplots(len(ids))

    #df.sort_values(by=[x_label], inplace=True)


    i = 0
    for id in ids:
        x = df[df['battery'] == id][x_label]
        y = df[df['battery'] == id][y_label]

        axs[i].plot(x, y, 'o-', markersize=1)
        axs[i].grid(True)
        #axs[i].set_xlim(max(x), min(x))  # decreasing
        i+=1


    for ax in axs.flat:
        ax.set(xlabel=x_label, ylabel=y_label)

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()

def plot_multiple_batteriedata_2D(df_1, df_2, x_label, y_label):
    ids = df_1['battery'].unique()
    fig, axs = plt.subplots(len(ids), 2)

    maximum = max(df_1[y_label])
    minimum = min(df_2[y_label])

    #df.sort_values(by=[x_label], inplace=True)
    i = 0
    for id in ids:

        x = df_1[df_1['battery'] == id][x_label]
        y = df_1[df_1['battery'] == id][y_label]
        axs[i, 0].plot(x, y, 'o-', markersize=1)
        axs[i, 0].grid(True)
        axs[i, 0].set_title(str(id))
        axs[i, 0].set_ylim(minimum, maximum)

        x = df_2[df_2['battery'] == id][x_label]
        y = df_2[df_2['battery'] == id][y_label]
        axs[i, 1].plot(x, y, 'ro-', markersize=1)
        axs[i, 1].grid(True)
        axs[i, 1].set_title(str(id))
        axs[i, 1].set_ylim(minimum, maximum)

        i+=1

    i = 0
    for ax in axs.flat:
        if i == 0:
            ax.set(xlabel=x_label+' for train', ylabel=y_label)
            i +=1
        else:
            ax.set(xlabel=x_label + ' for test', ylabel=y_label)
            i -=1

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()

def plot_three_batteries(df_1, x_label, y_label, df_2 = None):

    if df_2 is not None:
        df_1 = pd.concat([df_1, df_2])

    ids = df_1['battery'].unique()
    featurename = []

    for id in ids:
        x = df_1[df_1['battery'] == id][x_label]
        y = df_1[df_1['battery'] == id][y_label]
        plt.plot(x, y, linestyle='-', marker='o', markersize=3)
        featurename.append('battery ' + str(id))

    plt.legend(featurename)
    plt.axvline(x=100, color='black')
    plt.grid()
    plt.show()

def plot_batteries_and_model(df_1, model_data, x_label, y_label):

    ids = df_1['battery'].unique()
    featurename = []

    for id in ids:
        x = df_1[df_1['battery'] == id][x_label]
        y = df_1[df_1['battery'] == id][y_label]
        plt.plot(x, y, linestyle='-', marker='o', markersize=3)
        featurename.append('battery ' + str(id))

    #model
    plt.plot(range(100), model_data[1], marker='o', markersize=3)
    featurename.append('SVR-model')

    plt.legend(featurename)
    plt.axvline(x=100, color='black')
    plt.grid()
    plt.show()


def plot_multiple_batteriedata_2D_with_model(df_1, df_2, x_label, y_label, modeldata):
    ids = df_1['battery'].unique()
    fig, axs = plt.subplots(len(ids), 2)

    maximum = max(df_1[y_label])
    minimum = min(df_2[y_label])

    #df.sort_values(by=[x_label], inplace=True)
    i = 0
    train_start = 0
    test_start = 0
    train_end = 0
    test_end = 0

    if len(ids) == 1:
        x = df_1[df_1['battery'] == ids[0]][x_label]
        y = df_1[df_1['battery'] == ids[0]][y_label]

        train_end += len(x)

        axs[0].plot(x, y, 'o-', markersize=1)
        axs[0].plot(x, modeldata[1][train_start:train_end], 'o-', markersize=1)
        axs[0].grid(True)
        axs[0].set_title('battery '+str(ids[0])+' train')
        axs[0].set_ylim(minimum, maximum)


        x = df_2[df_2['battery'] == ids[0]][x_label]
        y = df_2[df_2['battery'] == ids[0]][y_label]

        test_end += len(x)

        axs[1].plot(x, y, 'ro-', markersize=1)
        axs[1].plot(x, modeldata[3][test_start:test_end], 'o-', markersize=1)
        axs[1].grid(True)
        axs[1].set_title('battery '+str(ids[0])+' test')
        axs[1].set_ylim(minimum, maximum)


    else:

        for id in ids:

            x = df_1[df_1['battery'] == id][x_label]
            y = df_1[df_1['battery'] == id][y_label]

            train_end += len(x)

            axs[i, 0].plot(x, y, 'o-', markersize=1)
            axs[i, 0].plot(x, modeldata[1][train_start:train_end], 'o-', markersize=1)
            axs[i, 0].grid(True)
            axs[i, 0].set_title('battery '+str(id)+' train')
            axs[i, 0].set_ylim(minimum, maximum)

            train_start = train_end

            x = df_2[df_2['battery'] == id][x_label]
            y = df_2[df_2['battery'] == id][y_label]

            test_end += len(x)

            axs[i, 1].plot(x, y, 'ro-', markersize=1)
            axs[i, 1].plot(x, modeldata[3][test_start:test_end], 'o-', markersize=1)
            axs[i, 1].grid(True)
            axs[i, 1].set_title('battery '+str(id)+' train')
            axs[i, 1].set_ylim(minimum, maximum)

            test_start = test_end

            i+=1

    for ax in axs.flat:
        ax.set(xlabel=x_label, ylabel=y_label)

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    plt.show()

if __name__ == '__main__':

    #test setup:
    x = [1, 2, 3, 4, 5]
    y = [1, 2, 1, 2, 3]
    #name = "test setup"

    #plot(name, x, y)
    #plot_error_percentage(y, [0, 1, 4])
    plot_true_vs_predicted(x, y)