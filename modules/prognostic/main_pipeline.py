#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 15.09.20
Author: Tomy Hommel @TUD
"""
import pandas as pd
import numpy as np
import modules.prognostic.preprocessing.data_selection as ds
import modules.prognostic.preprocessing.feature_engineering as fe
import modules.prognostic.preprocessing.normalization as nm
import modules.prognostic.preprocessing.data_labeling as dl
import modules.prognostic.preprocessing.dimensionality_reduction as dr

import modules.prognostic.preprocessing.battery_wise_processing as bwp

import modules.prognostic.prediction_approaches.gpr as gpr
import modules.prognostic.prediction_approaches.arima as arima
import modules.prognostic.prediction_approaches.svr as svr
import modules.prognostic.prediction_approaches.soh_svr as soh_svr
import modules.prognostic.evaluation.evaluate_results as eval

"""
this pipeline consists of different steps:

1)Data-Selection:
- single profile data (B05, B06, B07, B18) -> RUL prediction for 30% fade and SOH prediction
- multiple profile data (B05, B06, B07, B18, B25, B26, B27, B28, B29, B30, B31, B32, B33, B34, B36, B38, B39, B40) -> SOH prediction
- all data -> SOH prediction

2)Feature Extraction:
- TIESVD/TIEDVD - by Zhao2018
- Peak-Values(8), Skewness(2), Kortosis(2), Convex/Concav(2), Fluctuation(2), Curvature(2), Energy-of_Signal(2), Capacity(1) - by Partil 2015
- real measured capacity
- all above features 

#intermediate step: now split into train and test data 

3)Normalization:
- nothing
- z-score standardization
- min-max scaling

4)Dimensionality_Reduction
- nothing
- manuelle Selektion
- pca (with explained-variance of 0.90)
- pca (with explained-variance of 0.10 for each feature)

5) Modelling
- SVR -> RBF
- GPR 
- ARIMA -statistisch
- EMD - ARIMA - signaltheoretische 
- ARD 


6) Validation:
- RMSE
- MAPE
- AE
- RE

"""
pd.set_option("display.max_rows", None, "display.max_columns", None)

def soh_prediction_pipe():
    """
    pipe for battery soh analysis
    """
    evaluation = eval.Eval_SOH()

    for battery_data_selection in [ds.DataSelection_single(), ds.DataSelection_multiple(), ds.DataSelection_all()]:

        for selected_features in [fe.FeatureExtraction_Zhao2018(), fe.FeatureExtraction_Partil2015(),
                                  fe.FeatureExtraction_All()]:

            data = ds.get_df(battery_data_selection, selected_features)

            # data labeling:
            data = dl.label_data(data, battery_data_selection.battery_names)

            # split into train and test (battery-wise)
            train_data, test_data = ds.split_data(data)

            for scaler in [nm.FeatureScaler_MinMax(), nm.FeatureScaler_Standard(), nm.FeatureScaler_do_nothing()]:

                scaler.origin_data = train_data
                train_data = scaler.scale(train_data, ignore_columns=['id', 'cycle', 'rul_label', 'soh_label', 'fail_within_10_cycles'])
                # scale from train measures:
                test_data = scaler.compute(test_data)

                for dim_reduction in [dr.PCA_010(), dr.PCA(), dr.do_nothing()]:

                    dim_reduction.import_data(ignore_columns=['id', 'cycle', 'rul_label', 'soh_label', 'fail_within_10_cycles'], data = train_data)
                    train_data = dim_reduction.fit()
                    #pca on test from train eigenvalues:
                    test_data = dim_reduction.compute(test_data)

                    for soh_prediction_method in [soh_svr.mySOH_SVR(train_data, test_data, scaler, kernel='rbf'),
                                                  soh_svr.mySOH_SVR(train_data, test_data, scaler, kernel='mlp')]:
                        print(
                            battery_data_selection.name + ' | ' + selected_features.name + ' | ' + scaler.name + ' | ' + dim_reduction.name)

                        soh_prediction_method.train()#plot=True)
                        result = soh_prediction_method.predict()
                        setup = [battery_data_selection.name, selected_features.name, scaler.name, dim_reduction.name, soh_prediction_method.name]
                        evaluation.result_stats(result, setup)

    evaluation.export_results()


def rul_prediction_pipe():
    """
    pipe for battery rul analysis
    """
    evaluation = eval.Eval_RUL()
    real_cap_scaler_array = []

    for battery_data_selection in [ds.DataSelection_single()]: #use b05, b06, b07, b18

        for selected_features in [fe.FeatureExtraction_RealCapacity(), fe.FeatureExtraction_Zhao2018()]: #import capacity and sensor-data

            data = ds.get_df(battery_data_selection, selected_features)
            x_label = selected_features.x_label

            # data labeling:
            data = dl.label_data(data, battery_data_selection.battery_names)

            data_batterywise = bwp.split_by_battery(data)

            for scaler in [nm.FeatureScaler_MinMax()]: #scaler

                scaler.x_label = x_label

                for dim_reduction in [dr.manual_selection(feature='TIEDVD_scaled'),  dr.manual_selection(feature='TIECVD_scaled'), dr.manual_selection(feature='both')]:


                    for split in [75, 95]: #prediction_start_cycle

                        batterywise_train, batterywise_test = bwp.split_by_cycle(data_batterywise, split)
                        battery_id_list = bwp.extract_battery_id(data_batterywise)


                        for i in range(len(batterywise_train)):

                            scaler.origin_data = batterywise_train[i]
                            batterywise_train[i] = scaler.scale(batterywise_train[i],
                                                                ignore_columns=['id', 'rul_label',
                                                                                'fail_within_10_cycles'])
                            # scale from train measures:
                            batterywise_test[i] = scaler.compute(batterywise_test[i])



                            if selected_features.name == 'real_capacity' and dim_reduction.name != 'manual_selection_both' and dim_reduction.name != 'manual_selection_TIECVD_scaled':

                                dim_reduction.x_label = ['real_cap_scaled']
                                dim_reduction.name = 'real_cap'

                            elif selected_features.name == 'real_capacity':

                                break

                            else:

                                dim_reduction.x_label = scaler.x_label
                                dim_reduction.import_data(
                                        ignore_columns=['id', 'cycle_scaled', 'rul_label', 'soh_label_scaled',
                                                        'fail_within_10_cycles'],
                                        data=batterywise_train[i])
                                batterywise_train[i] = dim_reduction.fit()
                                batterywise_test[i] = dim_reduction.compute(batterywise_test[i])


                            #use 5 extrapolation methods
                            for rul_prediction_method in [svr.mySVR(batterywise_train[i], batterywise_test[i], scaler, 'ard', y_label=dim_reduction.x_label[0]),
                                                          gpr.GPR(batterywise_train[i], batterywise_test[i], scaler, y_label=dim_reduction.x_label[0]),
                                                          arima.myARIMA(batterywise_train[i], batterywise_test[i], scaler, x_label=dim_reduction.x_label, emd=True),
                                                          svr.mySVR(batterywise_train[i], batterywise_test[i], scaler, 'rbf', y_label=dim_reduction.x_label[0]),
                                                          arima.myARIMA(batterywise_train[i], batterywise_test[i], scaler, x_label=dim_reduction.x_label)
                                                          ]:

                                print('split_cycle:' + str(
                                    split) + '  & Scaler:' + scaler.name + '  & Dim-Redukrtion:' + dim_reduction.name + '  & Extrapolation:' + rul_prediction_method.name + '  & Battery:' + str(
                                    battery_id_list[i]))

                                rul_prediction_method.split_at_cycle = split
                                rul_prediction_method.train()#plot=True)
                                result, error_cycle = rul_prediction_method.predict()#plot=True)

                                if selected_features.name == 'real_capacity' and len(real_cap_scaler_array) != 4:
                                    real_cap_scaler_array.append(scaler.scaler_dict['real_cap_scaled'])

                                result['y_true'] = real_cap_scaler_array[i].inverse_transform(np.array(result['y_true']).reshape(-1, 1))
                                result['y_pred'] = real_cap_scaler_array[i].inverse_transform(np.array(result['y_pred']).reshape(-1, 1))

                                setup = [split, rul_prediction_method.name, dim_reduction.name]
                                evaluation.result_stats(battery_id_list[i], result, error_cycle, scaler, setup)


    evaluation.combine_results()
    evaluation.print_results()
    evaluation.export_results()


if __name__ == '__main__':
    #soh_prediction_pipe()
    rul_prediction_pipe()



