#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 28.10.20 
Author: Tomy Hommel @TUD
"""

from modules.prognostic.evaluation import evaluation as ev
from modules.prognostic.evaluation import boxplot_results as bpr
import pandas as pd
from datetime import datetime
import csv

def export_dict(my_dict, source):
    with open(source, 'w') as f:
        w = csv.writer(f)
        w.writerows(my_dict.items())

class Eval_RUL():

    def __init__(self):
        self.evaluation_df = pd.DataFrame(
            columns=['model', 'split_at_cycle', 'battery_id', 'mape', 'rmse', 'ae', 're'])
        self.pred = 'rul'
        self.count = 0
        self.result_dict_rmse, self.result_dict_relativ_error, self.result_dict_absolut_error = self.create_dict()
        self.result_combined_rmse = pd.DataFrame(columns=['key', 'rmse', 'mape'])
        self.result_combined_re = pd.DataFrame(columns=['key', 're'])
        self.result_dict_soh_error = 0

    def create_dict(self):
        results_rmse = {}
        results_re = {}
        results_ae = {}
        for m in ['ard', 'gpr', 'emd-arima', 'arima', 'svr-rbf']:
            for s in ['manual_selection_TIECVD_scaled', 'manual_selection_TIEDVD_scaled', 'manual_selection_both', 'real_cap']:
                string = m+'-'+s
                results_rmse[string] = pd.DataFrame(columns=['y_true', 'y_pred'])
                results_re[string] = []
                results_ae[string] = []

        return results_rmse, results_re, results_ae


    def result_stats(self, id, result_list, error_cycle, scaler, setup):
        split, modelname, dim_red = setup

        if modelname == 'svr-ard':
            modelname = 'ard'

        stats = []

        rmse = ev.rmse(result_list['y_true'], result_list['y_pred'])
        stats.append(rmse)

        mape = ev.mape(result_list['y_true'], result_list['y_pred'])
        stats.append(mape)


        if id == 5.0:
            true_error_cycle = 123

        elif id == 6.0:
            true_error_cycle = 109

        elif id == 7.0:
            true_error_cycle = 165

        elif id == 18.0:
            true_error_cycle = 97

        df = result_list[result_list['cycle'] <= true_error_cycle]
        df = df.drop(['cycle'], axis=1)

        s = modelname + '-' + dim_red

        #do not use battery 5 for evaluation of results
        if id != 5.0:
            frames = [self.result_dict_rmse[s], df]
            self.result_dict_rmse[s] = pd.concat(frames)

        #if no rul result:
        if error_cycle != -1:

            absolut_prediction_error = ev.ae(true_error_cycle, error_cycle)
            stats.append(absolut_prediction_error)

            relativ_prediction_error = ev.re(true_error_cycle, error_cycle)
            stats.append(relativ_prediction_error)
            if id != 5.0:
                self.result_dict_relativ_error[s].append(relativ_prediction_error)
                self.result_dict_absolut_error[s].append(absolut_prediction_error)

        else:
            stats += ['-', '-']

        self.evaluation_df.loc[self.count] = [modelname, split,  id] + stats
        self.count += 1


    def combine_results(self):
        i = 0
        for key in self.result_dict_rmse.keys():

            rmse = ev.rmse(self.result_dict_rmse[key]['y_true'], self.result_dict_rmse[key]['y_pred'])
            mape = ev.mape(self.result_dict_rmse[key]['y_true'], self.result_dict_rmse[key]['y_pred'])
            self.result_combined_rmse.loc[i] = [key, rmse, mape]
            i += 1

        j = 0
        for key in self.result_dict_relativ_error.keys():
            length = len(self.result_dict_relativ_error[key])
            if length == 0:
                re = '-'
                self.result_combined_re.loc[j] = [key, re]
                j += 1
            else:
                re = sum(self.result_dict_relativ_error[key]) / length
                re = str(re)
                self.result_combined_re.loc[j] = [key, re]
                j += 1


        self.result_dict_soh_error = self.result_dict_rmse.copy()

        for key in self.result_dict_rmse.keys():

            soh_error = list(self.result_dict_rmse[key]['y_true'] - self.result_dict_rmse[key]['y_pred'])
            self.result_dict_soh_error[key] = soh_error



    def export_results(self):
        time = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        s1 = r'evaluation/results_rul_'+time+'.csv'
        self.evaluation_df.to_csv(s1)
        s2 = r'evaluation/results_combined_rmse_' + time + '.csv'
        self.result_combined_rmse.to_csv(s2)
        s3 = r'evaluation/results_combined_re_' + time + '.csv'
        self.result_combined_re.to_csv(s3)
        s4 = r'evaluation/results_soh_error_' + time + '.csv'
        export_dict(self.result_dict_soh_error, s4)
        s5 = r'evaluation/results_re_' + time + '.csv'
        export_dict(self.result_dict_relativ_error, s5)
        s6 = r'evaluation/results_ae_' + time + '.csv'
        export_dict(self.result_dict_absolut_error, s6)

        self.boxplot(s6, s5, s4)

    def print_results(self):
        print(self.result_dict_rmse)
        print(self.result_dict_relativ_error)
        print(self.result_combined_re)
        print(self.result_combined_rmse)
        print(self.evaluation_df)

    def boxplot(self, ae_source, re_source, soh_source):
        bpr.plot_AE_RE(ae_source, re_source)
        bpr.plot_SOH_ERROR(soh_source)


class Eval_SOH():

    def __init__(self):
        self.evaluation_df = pd.DataFrame(columns=['selected_data', 'features', 'scaler', 'dim_reduction', 'prediction_approach', 'mape', 'rmse'])
        self.pred = 'soh'
        self.count = 0

    def result_stats(self, results, setup):

        stats = []

        rmse = ev.rmse(results['y_true'], results['y_pred'])
        stats.append(rmse)

        mape = ev.mape(results['y_true'], results['y_pred'])
        stats.append(mape)

        print(results)
        print(setup)


        self.evaluation_df.loc[self.count] = setup + stats
        self.count += 1


    def export_results(self):
        source = r'results_'+self.pred+'.csv'
        self.evaluation_df.to_csv(source)

    def print_results(self):
        print(self.evaluation_df)