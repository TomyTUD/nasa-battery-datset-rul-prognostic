#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 06.11.20
Author: Tomy Hommel @TUD
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def plot_AE_RE(ae_source, re_source):
    df_ae = pd.read_csv(ae_source, header=None)
    df_re = pd.read_csv(re_source, header=None)

    ae_frame_by_method = []

    for method in ['svr-rbf', 'ard', 'arima', 'emd-arima', 'gpr']:
        ae = []
        for index, row in df_ae.iterrows():
            if row.iloc[0].startswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                ae = np.concatenate((ae, value_array))

        ae_frame_by_method.append(ae)

    ae_frame_by_hi = []

    for method in ['TIECVD_scaled', 'TIEDVD_scaled', 'both', 'real_cap']:
        ae = []
        for index, row in df_ae.iterrows():
            if row.iloc[0].endswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                ae = np.concatenate((ae, value_array))

        ae_frame_by_hi.append(ae)


    re_frame_by_method = []

    for method in ['svr-rbf', 'ard', 'arima', 'emd-arima', 'gpr']:
        re = []
        for index, row in df_re.iterrows():
            if row.iloc[0].startswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                re = np.concatenate((re, value_array))

        re_frame_by_method.append(re)

    re_frame_by_hi = []

    for method in ['TIECVD_scaled', 'TIEDVD_scaled', 'both', 'real_cap']:
        re = []
        for index, row in df_re.iterrows():
            if row.iloc[0].endswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                re = np.concatenate((re, value_array))

        re_frame_by_hi.append(re)


    fig, ([ax1, ax2], [ax3, ax4]) = plt.subplots(2, 2)
    ax1.set_title('methodenbasiert')
    ax1.set(xlabel='', ylabel='AE', axisbelow=True,
               xticklabels=['SVR', 'ARD', 'ARIMA', 'EMD-ARIMA', 'GPR'])
    b1 = ax1.boxplot(ae_frame_by_method)

    ax2.set_title('HI-basiert')
    ax2.set(xlabel='', ylabel='', axisbelow=True,
               xticklabels=['TIECVD', 'TIEDVD', 'TIECVD/TIEDVD', 'capacity'])
    b2 = ax2.boxplot(ae_frame_by_hi)

    #ax3.set_title('relativer Fehler - methodenbasiert')
    ax3.set(xlabel='', ylabel='RE', axisbelow=True,
               xticklabels=['SVR', 'ARD', 'ARIMA', 'EMD-ARIMA', 'GPR'])
    b3 = ax3.boxplot(re_frame_by_method)


    #ax4.set_title('relativer Fehler - HI-basiert')
    ax4.set(xlabel='', ylabel='', axisbelow=True,
               xticklabels=['TIECVD', 'TIEDVD', 'TIECVD/TIEDVD', 'capacity'])
    b4 = ax4.boxplot(re_frame_by_hi)

    for tick in ax1.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
        tick.label.set_rotation(10)

    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
        tick.label.set_rotation(10)

    for tick in ax3.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
        tick.label.set_rotation(10)

    for tick in ax4.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
        tick.label.set_rotation(10)

    for b in [b1, b2, b3, b4]:
        # change the color of its elements
        for _, line_list in b.items():
            for line in line_list:
                line.set_color('gray')

        for box in b['boxes']:
            box.set(color='black')

    fig.set_size_inches(10, 5)

    #fig.suptitle('Fehler der RUL Prognose', fontsize=16)
    plt.savefig('boxplot.pdf')
    #plt.show()

def plot_SOH_ERROR(soh_source):

    df_soh = pd.read_csv(soh_source, header=None)

    frame_by_method = []

    for method in ['svr-rbf', 'svr-rvm', 'arima', 'emd-arima', 'gpr']:
        soh = []
        for index, row in df_soh.iterrows():
            if row.iloc[0].startswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                soh = np.concatenate((soh, value_array))

        frame_by_method.append(soh)

    frame_by_hi = []

    for method in ['TIECVD_scaled', 'TIEDVD_scaled', 'both', 'real_cap']:
        soh = []
        for index, row in df_soh.iterrows():
            if row.iloc[0].endswith(method):
                s = row.iloc[1]
                s = s.replace(" ", "")
                s = s.replace("[", "")
                s = s.replace("]", "")
                value_array = s.split(',')
                value_array = np.array(value_array).astype(float)
                soh = np.concatenate((soh, value_array))

        frame_by_hi.append(soh)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.set_title('methodenbasiert')
    ax1.set(xlabel='', ylabel='Fehler - normalisiert', axisbelow=True,
            xticklabels=['SVR', 'ARD', 'ARIMA', 'EMD-ARIMA', 'GPR'])
    ax1.boxplot(frame_by_method)

    ax2.set_title('HI-basiert')
    ax2.set(xlabel='', ylabel='', axisbelow=True,
            xticklabels=['TIECVD', 'TIEDVD', 'TIECVD/TIEDVD', 'capacity'])
    ax2.boxplot(frame_by_hi)

    fig.suptitle('Fehler bei SOH-Prognose', fontsize=16)



    plt.show()

def mean(rmse_source, re_source):
    df_rmse = pd.read_csv(rmse_source, header=None)
    df_re = pd.read_csv(re_source, header=None)

    for i in [df_rmse, df_re]:
        va = np.array(i[2][1:])
        print(np.sort(np.array(va).astype(float)))
        print(np.mean(np.array(va).astype(float)))


if __name__ == '__main__':
    plot_AE_RE('ergebnisse/results_ae.csv', 'ergebnisse/results_re.csv')

