#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 20.08.20
Author: Tomy Hommel @TUD
"""
import scipy
from sklearn.metrics import mean_squared_error, r2_score
import numpy as np
from matplotlib import pyplot

def r2(real, pred):
    return r2_score(real, pred)

def mape(real, pred):
    real, pred = np.array(real), np.array(pred)
    return (sum(np.abs((real - pred) / real)) * 100) / len(real)

def sse(real, pred): #sum of squared errors
    return np.sum((np.array(real) - np.array(pred)) ** 2)

def mse(real, pred):
    return mean_squared_error(real, pred)

def rmse(real, pred): #Quadratwurzel des MSE
    real, pred = np.array(real), np.array(pred)
    return mean_squared_error(real, pred, squared=False)

def plot_error_distribution(error):
    """this function requires a numpy array with the variance of the predictions: Ypred - Yeol """
    unique, counts = np.unique(error, return_counts=True)
    pyplot.figure('error distribution')
    pyplot.bar(unique, counts)
    pyplot.show()

def calculate_error(prediction, real):
    """if the value of the RUL is positive the prediction was to pessimistic
        if the value of the RUL is negative the prediction was to optimistic"""
    return np.array(prediction)-np.array(real)

def rul(eol, prediction):
    return np.array(eol)-np.array(prediction)

def ae(eol, prediction):
    return np.array(eol)-np.array(prediction)

def re(eol, prediction):
    return abs(np.array(eol)-np.array(prediction)) / np.array(eol)

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h

if __name__ == "__main__": #Tests

    #Ya = [1, 2, 3]
    #Yp = [3, 4, 5]
    #
    #mse = mse(Ya, Yp)   # =4
    #rmse = rmse(Ya, Yp) # =2
    #sse = sse(Ya, Yp)   # =12
    #
    #print('MSE: %s \nRMSE: %s \nSSE: %s'% (mse, rmse, sse))
    #
    #plot_error_distribution([1,0,3,0,4,0,-1,0,-1,0,1,0,1,1,0])

    #data = [20, 30, 40, 70]
    #model = [30, 30, 60, 68]
    #error = calculate_error(model, data)
    #print(error)
    #print(data)
    #print(model)
    #
    #print(rmse(data, model))

    y_true = [1, 2, 3]
    y_pred = [2, 2, 2]
    print(r2(y_true, y_pred))