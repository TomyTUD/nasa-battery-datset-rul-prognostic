#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 20.11.20
Author: Tomy Hommel @TUD
"""

from modules.DataSet import Battery
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

def multiline(xs, ys, c, ax=None, **kwargs):

    """
    SOURCE: https://stackoverflow.com/questions/3823752/display-image-as-grayscale-using-matplotlib

    Plot lines with different colorings

    Parameters
    ----------
    xs : iterable container of x coordinates
    ys : iterable container of y coordinates
    c : iterable container of numbers mapped to colormap
    ax (optional): Axes to plot on.
    kwargs (optional): passed to LineCollection

    Notes:
        len(xs) == len(ys) == len(c) is the number of line segments
        len(xs[i]) == len(ys[i]) is the number of points for each line (indexed by i)

    Returns
    -------
    lc : LineCollection instance.
    """

    # find axes
    ax = plt.gca() if ax is None else ax

    # create LineCollection
    segments = [np.column_stack([x, y]) for x, y in zip(xs, ys)]
    lc = LineCollection(segments, **kwargs)

    # set coloring of line segments
    #    Note: I get an error if I pass c as a list here... not sure why.
    lc.set_array(np.asarray(c))

    # add lines to axes and rescale
    #    Note: adding a collection doesn't autoscalee xlim/ylim
    ax.add_collection(lc)
    ax.autoscale()
    return lc


def plot_single_cycle():
    cycle = 2

    charge = b.charge_discharge_dict[cycle]['charge_voltage_measured']
    discharge = b.charge_discharge_dict[cycle]['discharge_voltage_measured']
    charge_time = b.charge_discharge_dict[cycle]['charge_time']
    discharge_time = b.charge_discharge_dict[cycle]['discharge_time']


    colors = [0, 0.45, 0.65, 0.85]

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(10, 4.5)

    ax1.plot(charge_time, charge, markersize=0.5, color=str(colors[0]))

    ax2.plot(discharge_time, discharge, markersize=0.5, color=str(colors[0]))

    ax1.axhline(4.2, xmax=0.33, linestyle='--', color=str(colors[3]), zorder=1)
    ax1.axhline(3.8, xmax=0.07, linestyle='--', color=str(colors[3]), zorder=1)

    ax2.axhline(3.8, xmax=0.15, linestyle='--', color=str(colors[3]), zorder=1)
    ax2.axhline(3.6, xmax=0.4, linestyle='--', color=str(colors[3]), zorder=1)

    ax1.axvline(217, ymax=0.73, color=str(colors[2]), zorder=1)
    ax1.axvline(3236, ymax=0.95, color=str(colors[2]), zorder=1)

    ax2.axvline(430, ymax=0.73, color=str(colors[2]), zorder=1)
    ax2.axvline(1367, ymax=0.62, color=str(colors[2]), zorder=1)

    ax1.annotate('', xy=(270, 2.6), xytext=(3200, 2.6),
                 arrowprops={'arrowstyle': '<->'}, va='center')
    ax1.text(1100, 2.65, 'TIECVD', fontsize=8)

    ax2.annotate('', xy=(434, 2.6), xytext=(1360, 2.6),
                 arrowprops={'arrowstyle': '<->'}, va='center')
    ax2.text(670, 2.65, 'TIEDVD', fontsize=8)

    ax1.set_ylim(ax2.get_ylim())

    ax1.set(xlabel='Zeit in s')
    ax2.set(xlabel='Zeit in s')
    ax1.set(ylabel='Spannung in V')

    ax1.set_title('LI-A Ladungzyklus')
    ax2.set_title('LI-A Entladungzyklus')

    plt.show()
    #plt.savefig('voltage_cycle_seperat.pdf')


def plot_all_cycle():

    all = len(b.charge_discharge_dict.keys())
    fig, (ax1, ax2) = plt.subplots(1, 2)
    color_array = []

    for i in b.charge_discharge_dict.keys():
        cycle = i

        charge = b.charge_discharge_dict[cycle]['charge_voltage_measured']
        discharge = b.charge_discharge_dict[cycle]['discharge_voltage_measured']
        charge_time = b.charge_discharge_dict[cycle]['charge_time']
        discharge_time = b.charge_discharge_dict[cycle]['discharge_time']

        colors = i/all*0.85
        color_array.append(colors)



        if max(charge) < 5:
           ax1.plot(charge_time, charge, markersize=0.3, color=str(colors))

        ax2.plot(discharge_time, discharge, markersize=0.3, color=str(colors))
    fig.suptitle('Spannungsverläufe')
    fig.set_size_inches(10, 4)
    ax1.set_title('LI-A Ladung')
    ax2.set_title('LI-A Entladung')

    xs = []
    ys = []
    c = [1, all]

    lc = multiline(xs, ys, c=c, cmap='gray', lw=2)
    axcb = fig.colorbar(lc)
    axcb.set_label('Zyklus')

    ax1.set(xlabel='Zeit in s')
    ax2.set(xlabel='Zeit in s')
    ax1.set(ylabel='Spannung in V')

    plt.show()
    #plt.savefig('voltage_all.pdf')

if __name__ == '__main__':
    b = Battery('B0005')
    plot_all_cycle()
    plot_single_cycle()