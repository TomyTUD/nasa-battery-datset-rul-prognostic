#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 13.11.20
Author: Tomy Hommel @TUD
"""
from modules.DataSet import Battery
from modules.prognostic.preprocessing.feature_engineering import FeatureExtraction_Zhao2018, FeatureExtraction_RealCapacity
import matplotlib.pyplot as plt
import tbd.outlier_detection as out
import tbd.interpolation as inter
from modules.prognostic.preprocessing.correlation import correlation as cor
import pandas as pd

b = Battery('B0018')

x = FeatureExtraction_Zhao2018()

x.minV_cha = 3.8
x.maxV_cha = 4.2

x.maxV_dis = 3.8
x.minV_dis = 3.6

x.battery_list = [b]
x.extract_class_features()

#pd.set_option("display.max_rows", None, "display.max_columns", None)

y = FeatureExtraction_RealCapacity()
y.battery_list = [b]
y.extract_class_features()

tiecvd = x.extracted_features['TIECVD']
tiedvd = x.extracted_features['TIEDVD']
cap = y.extracted_features['real_cap']
cycles = y.extracted_features['cycle']

plt.figure('TIECVD vs. TIEDVD vs. Kapazität')


colors = [0, 0.45, 0.65, 0.85]

outliers = out.my_outlier_function(tiecvd)
tiecvd = inter.interpolation(tiecvd, outliers)
outliers = out.find_zeros(tiecvd)
tiecvd = inter.interpolation_gap(tiecvd, outliers)

threshold = (1.4 - min(cap)) / (max(cap)-min(cap))
print(threshold)

tiecvd = (tiecvd - min(tiecvd)) / (max(tiecvd)-min(tiecvd))
tiedvd = (tiedvd - min(tiedvd)) / (max(tiedvd)-min(tiedvd))
cap = (cap - min(cap)) / (max(cap)-min(cap))


plt.plot(cycles, tiecvd,  markersize=0.5, color=str(colors[0]), zorder=2)
plt.plot(cycles, tiedvd,  markersize=0.5, color=str(colors[1]), zorder=2)
plt.plot(cycles, cap,  markersize=0.5, color=str(colors[3]), zorder=2)

print(cor(tiecvd, tiedvd, cap))

plt.legend(['TIECVD', 'TIEDVD', 'capacity'])
plt.xlabel('Zyklen')
plt.ylabel('')

plt.show()
#plt.savefig('tiecvd_tiedvd.pdf')

