#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 10.10.20 
Author: Tomy Hommel @TUD
"""

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from tbd import zhao_2018_FVS
from modules.DataSet import Battery
import modules.prognostic.preprocessing.data_selection as ds
import numpy as np



def get_profile(list_of_batteries):
    for b in list_of_batteries:
        print(b.name)
        current = []
        dod = []
        for cycle in b.discharge_dict.keys():
            current.append(round(abs(np.min(b.discharge_dict[cycle].get('discharge_current_measured')))))
            dod.append(round(abs(np.min(b.discharge_dict[cycle].get('discharge_voltage_measured'))), 1))
        print(current)
        print(dod)

def plot(list_of_batteries, header='capacity data overview'):

    plt.figure(header)
    list_of_batteries_plotted = []
    max_x = 0

    colors = []
    k = 50

    for b in list_of_batteries:

        y = b.discharge_capacity_array
        x = list(range(len(y)))

        if len(x) > max_x:
            max_x = len(x)

        list_of_batteries_plotted.append(b.name)

        temp = 0
        a = []
        for i in b.charge_discharge_dict.keys():
            a.append(b.charge_discharge_dict[i]['charge_ambient_temperature'])
            temp += b.charge_discharge_dict[i]['charge_ambient_temperature']

        temp = temp/len(b.charge_discharge_dict.keys())
        temp = 1 - temp/k

        print(str(temp))

        plt.plot(x, y, markersize=0.5, color=str(temp))


    max_x = list(range(0, max_x+5))

    plt.fill_between(max_x, 1.4, 1.6, color='blue')
    colors.append(mpatches.Patch(color=str(1-(4/k)), label=str(4)+'°C'))
    colors.append(mpatches.Patch(color=str(1-(10.75/k)), label=str(4) +'°C & '+ str(22)+'°C'))
    colors.append(mpatches.Patch(color=str(1-(24/k)), label=str(24)+'°C'))
    colors.append(mpatches.Patch(color=str(1-(38.8/k)), label=str(24) +'°C & '+ str(44)+'°C'))
    colors.append(mpatches.Patch(color=str(1-(43/k)), label=str(44)+'°C'))
    plt.legend(handles=colors)
    plt.xlabel('Zyklen')
    plt.ylabel('Kapazität in Ah')

    plt.savefig('cap.png')

    return list_of_batteries_plotted

def plot_single(list_of_batteries, header='capacity data overview'):

    plt.figure(header)
    list_of_batteries_plotted = ['B0005', 'B0006', 'B0007', 'B0018']
    max_x = 0

    colors = [0, 0.45, 0.65, 0.85]
    i = 0

    for b in list_of_batteries:

        y = b.discharge_capacity_array
        x = list(range(len(y)))

        if len(x) > max_x:
            max_x = len(x)

        plt.plot(x, y, markersize=0.5, color=str(colors[i]), zorder=2)
        i += 1

    max_x = list(range(0, max_x + 5))
    #plt.fill_between(max_x, 1.4, 1.6, color='blue')
    plt.axvline(75, zorder=1)
    plt.axvline(95, zorder=1)
    plt.axhline(1.4, color='blue', zorder=1)
    plt.legend(list_of_batteries_plotted)
    plt.xlabel('Zyklen')
    plt.ylabel('Kapazität in Ah')

    plt.savefig('cap.pdf')



def plot_all_features():
    b = Battery('B0005')
    print(b.charge_discharge_dict[1].keys())

    fig, axs = plt.subplots(2, 5)
    i = 1
    f = 5
    f1 = 6
    axs[0, 0].plot(b.charge_discharge_dict[i]['charge_time'], (b.charge_discharge_dict[i]['charge_voltage_measured']-min(b.charge_discharge_dict[i]['charge_voltage_measured']))/(max(b.charge_discharge_dict[i]['charge_voltage_measured']-min(b.charge_discharge_dict[i]['charge_voltage_measured']))), color='grey')
    axs[0, 0].set_title('Spannung LI-A', fontsize=f1)
    axs[0, 1].plot(b.charge_discharge_dict[i]['charge_time'], b.charge_discharge_dict[i]['charge_current_measured'], color='grey')
    axs[0, 1].set_title('Stromstärke LI-A', fontsize=f1)
    axs[0, 2].plot( b.charge_discharge_dict[i]['charge_time'], b.charge_discharge_dict[i]['charge_temperature_measured'], color='grey')
    axs[0, 2].set_title('Temperatur LI-A', fontsize=f1)
    axs[0, 3].plot( b.charge_discharge_dict[i]['charge_time'], b.charge_discharge_dict[i]['charge_voltage_charge'], color='grey')
    axs[0, 3].set_title('Spannung Stromquelle', fontsize=f)
    axs[0, 4].plot(b.charge_discharge_dict[i]['charge_time'], b.charge_discharge_dict[i]['charge_current_charge'], color='grey')
    axs[0, 4].set_title('Stromstärke Stromquelle', fontsize=f)
    axs[1, 0].plot( b.charge_discharge_dict[i]['discharge_time'], (b.charge_discharge_dict[i]['discharge_voltage_measured']-min(b.charge_discharge_dict[i]['discharge_voltage_measured']))/(max(b.charge_discharge_dict[i]['discharge_voltage_measured']-min(b.charge_discharge_dict[i]['discharge_voltage_measured']))), color='grey')
    axs[1, 0].set_title('Spannung LI-A', fontsize=f1)
    axs[1, 1].plot(b.charge_discharge_dict[i]['discharge_time'], b.charge_discharge_dict[i]['discharge_current_measured']*(-1), color='grey')
    axs[1, 1].set_title('Stromstärke LI-A', fontsize=f1)
    axs[1, 2].plot( b.charge_discharge_dict[i]['discharge_time'], b.charge_discharge_dict[i]['discharge_temperature_measured'], color='grey')
    axs[1, 2].set_title('Temperatur LI-A', fontsize=f1)
    axs[1, 3].plot( b.charge_discharge_dict[i]['discharge_time'], b.charge_discharge_dict[i]['discharge_voltage_load'], color='grey')
    axs[1, 3].set_title('Spannung Verbraucher', fontsize=f)
    axs[1, 4].plot( b.charge_discharge_dict[i]['discharge_time'], b.charge_discharge_dict[i]['discharge_current_load']*(-1), color='grey')
    axs[1, 4].set_title('Stromstärke Verbraucher', fontsize=f)

    for ax in axs.flat:
        ax.set(xlabel='Zeiteinheit')
        ax.xaxis.get_label().set_fontsize(6)

    axs[0, 0].set(ylabel='Ladezyklus')
    axs[1, 0].set(ylabel='Entladezyklus')


    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()


    #plt.savefig('zeitreihe.pdf')
    plt.show()


if __name__ == '__main__':
    plot_single(ds.DataSelection_single().import_data(), ds.DataSelection_single().info)
    plot_all_features()

    battery_list = ds.DataSelection_all().import_data()
    for b in battery_list:
        print(b.name, b.eol)
        print(b.real_rul_timeseries)

