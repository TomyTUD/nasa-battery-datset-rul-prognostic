#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26.10.20
Author: Tomy Hommel @TUD
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from modules.prognostic.prediction_approaches.optimizer_soh import PSO_SVR
import modules.prognostic.preprocessing.battery_wise_processing as bwp

class mySOH_SVR():
    def __init__(self, train, test, scaler, kernel='rbf', eol=0.7):
        self.name = 'soh-svr-'+kernel
        self.kernel = kernel
        self.eol = eol
        self.battery_df_train = train
        self.battery_df_test = test
        self.scaler = scaler
        self.split_at_cycle = 0

        self.train_data, self.train_id, self.train_index, self.train_cycles = self._prepair_data(data=self.battery_df_train) #gets necessary features
        self.test_data, self.test_id, self.test_index, self.test_cycles = self._prepair_data(data=self.battery_df_test)


    def _prepair_data(self, data: pd.DataFrame):

        self.y_label = 'soh_label'
        self.x_label = data.columns.drop(['id', 'cycle',  'rul_label', 'fail_within_10_cycles'] + [self.y_label])

        battery_index_list = bwp.index_by_battery(data)
        battery_id = data['id'].unique()
        battery_data = data[self.x_label].merge(data[self.y_label], left_index=True, right_index=True)
        cycles = data['cycle']

        return battery_data, battery_id, battery_index_list, cycles

    def train(self, plot=False):

        train = self.train_data

        self.X_train = train[self.x_label]
        self.y_train = train[self.y_label]


        pso = PSO_SVR(self.kernel, self.X_train, self.y_train, eval='rmse')
        self.model, self.model_parameter, self.y_pred_train = pso.results()


        if plot:
            d = {'cycle': self.train_cycles,
                 'y_true': self.y_train,
                 'y_pred': self.y_pred_train}

            self.train_results = pd.DataFrame(data=d)

            self.plot_results('train')


    def predict(self, plot=False):

        test = self.test_data

        self.X_test = test[self.x_label]
        self.y_test = test[self.y_label]

        if self.X_train.shape[1] > 1:
            self.y_pred_test = self.model.predict(self.X_test)

        else:
            self.y_pred_test = self.model.predict(np.array(self.X_test).reshape(-1, 1))


        d = {'cycle': self.test_cycles,
             'y_true': self.y_test,
             'y_pred': self.y_pred_test}

        self.test_results = pd.DataFrame(data=d)

        if plot:
            self.plot_results('test')

        return self.test_results

    def plot_results(self, phase):

        plt.figure()

        darkcolors = ['darkgreen', 'red']
        lightcolors = ['lightgreen', 'orange']

        if phase == 'train':

            for i in range(len(self.train_id)):
                row = self.train_results.loc[self.train_index[i]]
                plt.plot(row['cycle'], row['y_true'], color=lightcolors[i], label='true values B' + str(int(self.train_id[i])))
                plt.plot(row['cycle'], row['y_pred'], '.', color=darkcolors[i], markersize=2, label='train B' + str(int(self.test_id[i])))


            plt.title(self.name+' - Train')

        if phase == 'test':

            for i in range(len(self.test_id)):

                row = self.test_results.loc[self.test_index[i]]
                plt.plot(row['cycle'], row['y_true'], color=lightcolors[i], label='true values B' + str(int(self.test_id[i])))
                plt.plot(row['cycle'], row['y_pred'], '.', color=darkcolors[i], markersize=2, label='prediction B' + str(int(self.test_id[i])))

            plt.title(self.name+' - Test')


        plt.xlabel('cycle')
        plt.ylabel('soh')
        plt.legend(loc='upper right')
        plt.show()

if __name__ == '__main__':

    import modules.prognostic.preprocessing.data_selection as ds
    from modules.prognostic.preprocessing.feature_engineering import FeatureExtraction_Zhao2018
    import modules.prognostic.preprocessing.data_labeling as dl
    import modules.prognostic.preprocessing.battery_wise_processing as bwp
    import modules.prognostic.preprocessing.normalization as nm
    from modules.prognostic.preprocessing.dimensionality_reduction import do_nothing

    battery = ds.DataSelection_specific(['B0033', 'B0034'])
    fs = FeatureExtraction_Zhao2018()
    data = ds.get_df_for_test(battery, fs)
    data = dl.label_data(data, battery.battery_names)
    train_data, test_data = ds.split_data(data)

    scaler = nm.FeatureScaler_MinMax()
    scaler.origin_data = train_data
    train_data = scaler.scale(train_data, ignore_columns=['id', 'cycle', 'rul_label', 'soh_label', 'fail_within_10_cycles'])
    test_data = scaler.compute(test_data)

    dim_reduction = do_nothing()
    dim_reduction.import_data(ignore_columns=['id', 'cycle', 'rul_label', 'soh_label', 'fail_within_10_cycles'], data=train_data)
    train_data = dim_reduction.fit()
    # pca on test from train eigenvalues:
    test_data = dim_reduction.compute(test_data)

    #prediction_method = mySOH_SVR(train_data, test_data, scaler, kernel='mlp')
    prediction_method = mySOH_SVR(train_data, test_data, scaler, kernel='rbf')
    prediction_method.train(plot=True)
    prediction_method.predict(plot=True)
