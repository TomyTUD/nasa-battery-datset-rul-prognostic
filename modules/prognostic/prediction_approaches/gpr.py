#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 19.10.20 
Author: Tomy Hommel @TUD
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel, ExpSineSquared, Matern, DotProduct


class GPR():

    def __init__(self, train, test, scaler, y_label='soh_label_scaled', eol=0.7):
        self.name = 'gpr'
        self.battery_df_train = train
        self.battery_df_test = test
        self.scaler = scaler
        self.split_at_cycle = 0
        self.eol = eol

        self.y_label = y_label

        self.train_data, self.train_id = self._prepair_data(data=self.battery_df_train)
        self.test_data, self.test_id = self._prepair_data(data=self.battery_df_test)
        self.true_values = self.battery_df_test['soh_label_scaled']


    def _prepair_data(self, data: pd.DataFrame):

        self.x_label = 'cycle_scaled'


        battery_id = int(data['id'].unique()[0])
        battery_data = data[[self.x_label, self.y_label]]

        return battery_data, battery_id


    def train(self, plot=False):

        train = self.train_data
        test = self.test_data

        self.X_train = train[self.x_label]
        self.y_train = train[self.y_label]
        self.X_test = test[self.x_label]
        self.y_test = test[self.y_label]


        # Instantiate a Gaussian Process model
        # 3 Kernels considered by Liu et. al 2012

        #diagonal squared exponential covariance function: rbf-kernel
        #periodic covariance function:  ExpSineSquared-kernel
        #constance covariance function: ConstantKernel
        #noise: WhiteKernel -> Note that this is equivalent to adding a WhiteKernel with c=alpha

        kernel = 0.05 * RBF(length_scale_bounds=(1.5, 2)) + ExpSineSquared(length_scale_bounds=(2.5, 3), periodicity_bounds=(0.5, 3)) #+ ConstantKernel(constant_value_bounds=(1e-5, 1e5))
        self.model = GaussianProcessRegressor(kernel=kernel, optimizer="fmin_l_bfgs_b", n_restarts_optimizer=100, normalize_y=True, alpha=0.05)

        # Fit to data using Maximum Likelihood Estimation of the parameters
        self.model_fit = self.model.fit(np.array(self.X_train).reshape(-1, 1), self.y_train)
        print(self.model.kernel_)


    def predict(self, plot=False):
        # Make the prediction on the meshed x-axis (ask for MSE as well)
        self.y_pred_test, self.sigma = self.model_fit.predict(np.array(self.X_test).reshape(-1, 1), return_std=True)


        d_plot = {'cycle': self.scaler.rescale_series(self.x_label, self.X_test)[:, 0],
             'y_true': self.scaler.rescale_series('soh_label_scaled', self.true_values)[:, 0],
             'y_pred': self.scaler.rescale_series('soh_label_scaled', self.y_pred_test)[:, 0]}

        d = {'cycle': self.scaler.rescale_series(self.x_label, self.X_test)[:, 0],
             'y_true': self.true_values,
             'y_pred': self.y_pred_test}

        self.test_results = pd.DataFrame(data=d_plot)

        df = self.test_results[self.test_results['y_pred'] < self.eol]

        if df.empty:
            print('DataFrame is empty!')
            error_cycle = self.test_results[self.test_results['y_pred'] == min(self.test_results['y_pred'])].iloc[0, 0]

        else:
            error_cycle = int(df.iloc[0, 0])

        if plot:
            self.plot_prediction()

        self.test_error_cycle = error_cycle

        return pd.DataFrame(data=d), error_cycle


    def plot_prediction(self):

        # Plot the function, the prediction and the 95% confidence interval based on MSE
        plt.figure()
        plt.title('gpr')
        plt.axvline(self.y_train.shape[0], color='lightgray')
        plt.axhline(self.eol, color='lightgray')


        plt.plot(self.scaler.rescale_series(self.x_label, self.X_train), self.scaler.rescale_series('soh_label_scaled', self.y_train), 'black', label='historical data')
        plt.plot(self.scaler.rescale_series(self.x_label, self.X_test), self.scaler.rescale_series('soh_label_scaled',  self.y_pred_test), 'b.', markersize=2, label='Prediction')
        plt.plot(self.scaler.rescale_series(self.x_label, self.X_test), self.scaler.rescale_series('soh_label_scaled', self.y_test), 'darkgray', label='true values')
        plt.fill(np.concatenate([self.scaler.rescale_series(self.x_label, self.X_test), self.scaler.rescale_series(self.x_label, self.X_test)[::-1]]),
                 np.concatenate([self.scaler.rescale_series('soh_label_scaled',  self.y_pred_test - 1.9600 * self.sigma),
                                self.scaler.rescale_series('soh_label_scaled', ( self.y_pred_test + 1.9600 * self.sigma)[::-1])]),
                                 alpha=.5, fc='b', ec='None', label='95% confidence interval')

        plt.xlabel('cycle')
        plt.ylabel('soh')
        plt.legend(loc='upper right')
        plt.savefig('gpr.pdf')
        #plt.show()


if __name__ == '__main__':

    import modules.prognostic.preprocessing.data_selection as ds
    from modules.prognostic.preprocessing.feature_engineering import FeatureExtraction_RealCapacity
    import modules.prognostic.preprocessing.data_labeling as dl
    import modules.prognostic.preprocessing.battery_wise_processing as bwp
    import modules.prognostic.preprocessing.normalization as nm

    battery = ds.DataSelection_specific(['B0036'])
    fs = FeatureExtraction_RealCapacity()
    data = ds.get_df_for_test(battery, fs)
    data = dl.label_data(data, battery.battery_names)
    data_batterywise = bwp.split_by_battery(data)
    cycle = 60
    batterywise_train, batterywise_test = bwp.split_by_cycle(data_batterywise, cycle)
    battery_id_list = bwp.extract_battery_id(data_batterywise)
    scaler = nm.FeatureScaler_MinMax()
    scaler.origin_data = batterywise_train[0]
    batterywise_train[0] = scaler.scale(batterywise_train[0], ignore_columns=['id', 'rul_label', 'fail_within_10_cycles'])
    batterywise_test[0] = scaler.compute(batterywise_test[0])
    prediction_method = GPR(batterywise_train[0], batterywise_test[0], scaler)
    prediction_method.split_at_cycle = cycle
    prediction_method.train(plot=True)
    result, error_cycle = prediction_method.predict(plot=True)

