#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 30.10.20 
Author: Tomy Hommel @TUD
"""
from pyswarm import pso
import numpy as np
from sklearn.svm import SVR
from modules.prognostic.utils.kernels import RBF, MLP
from modules.prognostic.evaluation.evaluation import rmse
from sklearn.model_selection import cross_val_score, cross_val_predict


class PSO_SVR():
    """https://pythonhosted.org/pyswarm/"""

    def __init__(self, kernel, X_train, y_train, eval='cross_val'):
        self.X_train = X_train
        self.y_train = y_train
        self.kernel = kernel
        self.eval = eval
        self.best_score = 1000

        lb = [0.000001, 9, 0.03]  # g, C, e
        ub = [1, 10, 0.3]

        xopt, fopt = pso(self.optimize_regression_features, lb, ub, maxiter=10, debug=True)

        print('Best Score: %1.4f with sigma: %1.4f, C: %1.4f, epsilon: %1.4f' % (fopt, xopt[0], xopt[1], xopt[2]))


    def optimize_regression_features(self, parameter_regression):

        g, C, e = parameter_regression

        score = 0

        if self.kernel == 'rbf':
            kernel = RBF(gamma=g)
        elif self.kernel == 'mlp':
            kernel = MLP(gamma=g)

        model = SVR(kernel=kernel, C=C, epsilon=e)

        model.fit(np.array(self.X_train).reshape(-1, 1), self.y_train)
        print('get number of support vectors: ', model.n_support_)
        y_pred_train = model.predict(np.array(self.X_train).reshape(-1, 1))


        if self.eval == 'cross_val': # Cross-Validation-Score:
            score = cross_val_score(model, np.array(self.X_train).reshape(-1, 1), self.y_train, cv=10)
            score = (np.mean(score) - 1) * (-1)  # transform score for minimizing

        elif self.eval == 'rmse': #RMSE-Score:
            score += rmse(self.y_train, y_pred_train)


        if self.best_score > score:
            self.model = model
            self.model_parameter = [g, C, e]
            self.y_pred_train = y_pred_train
            self.best_score = score

        return score

    def results(self):
        return self.model, self.model_parameter, self.y_pred_train