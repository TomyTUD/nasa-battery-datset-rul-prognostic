#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26.10.20 
Author: Tomy Hommel @TUD
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from modules.prognostic.prediction_approaches.optimizer_soh import PSO_SVR
from modules.prognostic.prediction_approaches.ard import ARD

class mySVR():
    def __init__(self, train, test, scaler, kernel='rbf', y_label = 'soh_label_scaled', eol=0.7):
        self.name = 'svr-'+kernel
        self.kernel = kernel
        self.eol = eol
        self.battery_df_train = train
        self.battery_df_test = test
        self.scaler = scaler
        self.split_at_cycle = 0
        self.y_label = y_label

        self.train_data, self.train_id = self._prepair_data(data=self.battery_df_train) #gets necessary features
        self.test_data, self.test_id = self._prepair_data(data=self.battery_df_test)

        self.true_values = self.battery_df_test['soh_label_scaled']


    def _prepair_data(self, data: pd.DataFrame):

        self.x_label = 'cycle_scaled'


        battery_id = int(data['id'].unique()[0])
        battery_data = data[[self.x_label, self.y_label]]

        return battery_data, battery_id

    def train(self, plot=False):

        train = self.train_data
        test = self.test_data

        self.X_train = train[self.x_label]
        self.y_train = train[self.y_label]
        self.X_test = test[self.x_label]
        self.y_test = test[self.y_label]

        # using a Pyrticle Swarm Optimizer (Qin 2015)
        if not self.kernel.startswith('ard'):
            pso = PSO_SVR(self.kernel, self.X_train, self.y_train)
            self.model, self.model_parameter, self.y_pred_train = pso.results()

        else:
            ard_reg = ARD(self.X_train, self.y_train)
            self.model, self.model_parameter, self.model_std, self.y_pred_train = ard_reg.results()

    def predict(self, plot=False):

        self.y_pred_test = self.model.predict(np.array(self.X_test).reshape(-1, 1))

        if self.kernel == 'ard':
            dif = np.array(self.y_train)[-1] - np.array(self.y_pred_test)[0]
            self.y_pred_test = self.y_pred_test+dif


        d_plot = {'cycle': self.scaler.rescale_series(self.x_label, self.X_test)[:, 0],
             'y_true': self.scaler.rescale_series('soh_label_scaled', self.true_values)[:, 0],
             'y_pred': self.scaler.rescale_series('soh_label_scaled', self.y_pred_test)[:, 0]}

        d = {'cycle': self.scaler.rescale_series(self.x_label, self.X_test)[:, 0],
             'y_true': self.true_values,
             'y_pred': self.y_pred_test}


        self.test_results = pd.DataFrame(data=d_plot)

        df = self.test_results[self.test_results['y_pred'] < self.eol]

        if df.empty:
            print('DataFrame is empty!')
            error_cycle = self.test_results[self.test_results['y_pred'] == min(self.test_results['y_pred'])].iloc[0,0]

        else:
            error_cycle = int(df.iloc[0, 0])

        if plot:
            self.plot_results(phase='test')

        self.test_error_cycle = error_cycle


        return pd.DataFrame(data=d), self.test_error_cycle

    def plot_results(self, phase):

        plt.figure()

        if phase == 'train':
            plt.plot(self.scaler.rescale_series(self.x_label, self.X_train),
                     self.scaler.rescale_series(self.y_label, self.y_train), 'black', label='historical data')

            plt.plot(self.scaler.rescale_series(self.x_label, self.X_train),
                     self.scaler.rescale_series(self.y_label, self.y_pred_train), 'b.',
                     markersize=4, label='fited curve')

            plt.title(self.name+' - Train')

        if phase == 'test':

            self.y_label = 'soh_label_scaled'

            plt.plot(self.scaler.rescale_series(self.x_label, self.X_train),
                     self.scaler.rescale_series(self.y_label, self.y_train), 'black', label='historical data')

            plt.plot(self.scaler.rescale_series(self.x_label, self.X_test),
                     self.scaler.rescale_series(self.y_label, self.y_test), 'darkgray', label='true values')

            if self.kernel != 'ard':
                plt.plot(self.scaler.rescale_series(self.x_label, self.X_train),
                    self.scaler.rescale_series(self.y_label, self.y_pred_train), '.', color='lightgray',
                    markersize=2, label='fited curve')

            plt.plot(self.scaler.rescale_series(self.x_label, self.X_test),
                     self.scaler.rescale_series(self.y_label, self.y_pred_test), 'b.', markersize=2, label='prediction')

            plt.title(self.name+' - Test')


        plt.axvline(self.y_train.shape[0], color='lightgray')
        plt.axhline(self.eol, color='lightgray')
        plt.xlabel('cycle')
        plt.ylabel('soh')
        plt.legend(loc='upper right')
        plt.savefig(self.name+'.pdf')
        #plt.show()

if __name__ == '__main__':

    import modules.prognostic.preprocessing.data_selection as ds
    from modules.prognostic.preprocessing.feature_engineering import FeatureExtraction_RealCapacity
    import modules.prognostic.preprocessing.data_labeling as dl
    import modules.prognostic.preprocessing.battery_wise_processing as bwp
    import modules.prognostic.preprocessing.normalization as nm

    battery = ds.DataSelection_specific(['B0036'])
    fs = FeatureExtraction_RealCapacity()
    data = ds.get_df_for_test(battery, fs)
    data = dl.label_data(data, battery.battery_names)
    data_batterywise = bwp.split_by_battery(data)
    cycle = 80
    batterywise_train, batterywise_test = bwp.split_by_cycle(data_batterywise, cycle)
    battery_id_list = bwp.extract_battery_id(data_batterywise)
    scaler = nm.FeatureScaler_MinMax()
    scaler.origin_data = batterywise_train[0]
    batterywise_train[0] = scaler.scale(batterywise_train[0], ignore_columns=['id', 'rul_label', 'fail_within_10_cycles'])
    batterywise_test[0] = scaler.compute(batterywise_test[0])
    prediction_method = mySVR(batterywise_train[0], batterywise_test[0], scaler, kernel='ard')
    prediction_method.split_at_cycle = cycle
    prediction_method.train(plot=True)
    result, error_cycle = prediction_method.predict(plot=True)
