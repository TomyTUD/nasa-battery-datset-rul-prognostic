#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26.10.20 
Author: Tomy Hommel @TUD
"""


import numpy as np
from sklearn.linear_model import ARDRegression

class ARD():

    def __init__(self, X_train, y_train):
        self.X_train = X_train
        self.y_train = y_train

        self.train()


    def train(self):

        self.model = ARDRegression(compute_score=True, alpha_1=5, alpha_2=5, lambda_1=100, lambda_2=100)
        self.model.fit(np.array(self.X_train).reshape(-1, 1), self.y_train)

        y_pred_train, self.model_std = self.model.predict(np.array(self.X_train).reshape(-1, 1), return_std=True)

        self.model_parameter = []
        self.y_pred_train = y_pred_train



    def results(self):
        return self.model, self.model_parameter, self.model_std, self.y_pred_train