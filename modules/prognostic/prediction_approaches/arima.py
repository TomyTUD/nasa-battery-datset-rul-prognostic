#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 21.09.20
Author: Tomy Hommel @TUD
"""
import numpy as np
import pandas as pd
from modules.prognostic.preprocessing.empirical_mode_decomposition import my_EMD
from statsmodels.tsa.arima.model import ARIMA
from statsmodels.tsa.stattools import acf as ACF, pacf as PACF
from matplotlib import pyplot as plt

class myARIMA():

    def __init__(self, train, test, scaler, x_label, emd=False, eol = 0.7):

        self.battery_df_train = train
        self.battery_df_test = test
        self.scaler = scaler
        self.split_at_cycle = 0
        self.kernel_hyperparameter = 0

        self.use_emd = emd


        self.x_label = x_label[0]
        self.eol = eol



        if self.use_emd:
            self.name = 'emd-arima'
        else:
            self.name = 'arima'


        self.train_data, self.train_id = self._prepair_data(data=self.battery_df_train) #gets necessary features
        self.test_data, self.test_id = self._prepair_data(data=self.battery_df_test)

        self.true_values = self.battery_df_test['soh_label_scaled']





    def _prepair_data(self, data: pd.DataFrame):

        self.y_label = 'cycle_scaled'

        battery_id = int(data['id'].unique()[0])
        battery_data = data[[self.x_label, self.y_label]]

        return battery_data, battery_id


    def train(self, plot=False):

        train = self.train_data
        test = self.test_data

        i = 2  # integration of power 2



        self.X_train = train[self.x_label]
        self.y_train = train[self.y_label]
        self.X_test = test[self.x_label]
        self.y_test = test[self.y_label]

        if self.use_emd:
            decomp = my_EMD()
            decomp.import_data(train)
            decomp.extract_IMFS()#plot=True)
            self.models_fit = []
            for k in range(len(decomp.imfs)):
                new_series = pd.Series(decomp.imfs[k])
                if k < len(decomp.imfs)-1:
                    pass
                    ar, ma = self._differencing(new_series, power=0)#, plot=plot)
                    print('AR: ', str(ar), '\nI: ', str(0), '\nMA: ', str(ma))
                    used_i = 0

                else:

                    new_series = self.X_train
                    used_i = 2
                    ar, ma = self._differencing(new_series, power=used_i)#, plot=plot)
                    print('AR: ', str(ar), '\nI: ', str(used_i), '\nMA: ', str(ma))


                history = np.array(new_series[:-5]).reshape(-1, 1)

                model = ARIMA(history, order=(ar, used_i, ma))
                model_fit = model.fit()
                #print(model_fit.summary().tables[1])
                #model_fit.plot_diagnostics()
                plt.show()
                self.models_fit.append(model_fit)

        else:

            ar, ma = self._differencing(self.X_train, power=i)#, plot=plot)
            print('AR: ', str(ar), '\nI: ', str(i), '\nMA: ', str(ma))

            history = np.array(self.X_train).reshape(-1, 1)

            self.model = ARIMA(history, order=(ar, i, ma))
            self.model_fit = self.model.fit()


    def predict(self, plot=False):

        n = len(self.y_test)

        if self.use_emd:

            for l in range(len(self.models_fit)):
                if l == 0:
                    forcast_of_soh = self.models_fit[l].forecast(steps=n+5)
                    forcast_of_soh = forcast_of_soh[5:]

                else:
                    sub_soh = self.models_fit[l].forecast(steps=n+5)
                    forcast_of_soh += sub_soh[5:]

            difference = np.array(self.X_train)[-1] - forcast_of_soh[0]
            forcast_of_soh += difference


        else:
            forcast_of_soh = self.model_fit.forecast(steps=n) #forcasting of SOH n steps ahead


        if plot:
            self._plot_prediction(self.X_train, forcast_of_soh, self.X_test)

        forcast_of_soh = forcast_of_soh[-len(self.true_values):]
        tests = self.y_test
        tests = tests[-len(self.true_values):]

        d_plot = {'cycle': self.scaler.rescale_series(self.y_label, tests)[:, 0],
             'y_true': self.scaler.rescale_series('soh_label_scaled', self.true_values)[:, 0],
             'y_pred': self.scaler.rescale_series('soh_label_scaled', forcast_of_soh)[:, 0]}

        d = {'cycle': self.scaler.rescale_series(self.y_label, tests)[:, 0],
             'y_true': self.true_values,
             'y_pred': forcast_of_soh}

        self.test_results = pd.DataFrame(data=d_plot)


        df = self.test_results[self.test_results['y_pred'] < self.eol]
        df_out = pd.DataFrame(data=d)


        if df.empty:
            print('DataFrame is empty!')

            if self.use_emd:

                frames = [self.train_data[-1:], self.test_data]
                self.test_data = pd.concat(frames)
                self.train_data = self.train_data[:-1]
                self.train()
                self.test_results, error_cycle = self.predict()
                df_out = self.test_results

            else:
                next = self.model_fit.forecast(steps=n * 2) #further extrapolation
                next = self.scaler.rescale_series('soh_label_scaled', next)[:, 0]

                search_for_eol = True
                k = 0
                while search_for_eol:
                    print(k)
                    if next[k] < self.eol:
                        search_for_eol = False
                        error_cycle = self.test_results.iloc[0, 0] + k
                    else:
                        k += 1
        else:
            error_cycle = int(df.iloc[0, 0])


        return df_out, error_cycle

    def _plot_prediction(self, X_train, forcast_of_soh, X_test):


        plt.figure()
        plt.title('arima')
        plt.axvline(self.y_train.shape[0], color='lightgray')
        plt.axhline(self.eol, color='lightgray')
        plt.plot(self.scaler.rescale_series(self.y_label, self.y_train), self.scaler.rescale_series('soh_label_scaled', X_train), 'black', label='historical data')
        plt.plot(self.scaler.rescale_series(self.y_label, self.y_test), self.scaler.rescale_series('soh_label_scaled', forcast_of_soh), 'b.', markersize=2, label='Prediction')
        plt.plot(self.scaler.rescale_series(self.y_label, self.y_test), self.scaler.rescale_series('soh_label_scaled', X_test), 'darkgray', label='true values')
        plt.xlabel('cycle')
        plt.ylabel('soh')
        plt.legend(loc='upper right')
        plt.savefig(self.name+'.pdf')
        #plt.show()


    def _differencing(self, ts, power, plot=False):

        for i in range(power): #differencing the timeseries

            diff = ts - ts.shift()
            ts = diff.dropna()


        mean = np.mean(ts)
        sigma = np.std(ts)
        textstr = '\n'.join((r'$\mathrm{mean}=%.4f$' % (mean,), r'$\sigma=%.4f$' % (sigma,)))

        if plot:
            fig, ax = plt.subplots()
            plt.axhline(mean, color='lightgrey')
            plt.title('Time Series after differencing with power of ' + str(power))
            ax.plot(ts, color='#333333')
            plt.text(0, max(ts)*0.9, textstr)
            plt.show()


        # DIAGNOSING ACF and PACF
        nlags = 25
        lags = list(range(1, nlags+1))
        alpha = 0.05 #95 % confidence intervals
        nobs = len(ts)

        acf_ = ACF(ts, nlags=nlags, alpha=alpha, fft=True) # 95% CI is returned where the standard deviation is computed according to Bartlett"s formula.
        acf_elements = acf_[0][1:]
        acf_confint = acf_[1][1:]
        lower_acf_ci = acf_confint[:, 0] - acf_elements
        upper_acf_ci = acf_confint[:, 1] - acf_elements

        pacf_ = PACF(ts, nlags=nlags, alpha=alpha) # 95% CI is returned where the standard deviation is computed according to 1/sqrt(len(x))
        pacf_elements = pacf_[0][1:]
        pacf_confint = pacf_[1][1:]
        lower_pacf_ci = pacf_confint[:, 0] - pacf_elements
        upper_pacf_ci = pacf_confint[:, 1] - pacf_elements


        if plot:
            fig, (ax1, ax2) = plt.subplots(2, sharex=True)

            #PACF-Plot

            fig.suptitle('ACF & PACF Plot after differencing with power of '+ str(power))

            i = 0
            for x in pacf_elements:
                ax1.vlines(lags[i], 0, x, colors='black')
                i += 1
            ax1.axhline(0, 0, nlags, color='grey')
            lags_plot = lags.copy()
            lags_plot[0] -= 0.5
            lags_plot[-1] += 0.5
            ax1.fill_between(lags_plot, lower_pacf_ci, upper_pacf_ci, color='lightgrey')
            ax1.set_ylabel('PACF')


            #ACF-Plot
            i = 0
            for x in acf_elements:
                ax2.vlines(lags[i], 0, x, colors='black')
                i += 1
            ax2.axhline(0, 0, nlags, color='grey')
            ax2.fill_between(lags_plot, lower_acf_ci, upper_acf_ci, color='lightgrey')
            ax2.set_ylabel('ACF')
            ax2.set_xlabel('lags')

            plt.show()

        #get best PACF:
        j = 0

        while not lower_pacf_ci[j] <= pacf_elements[j] <= upper_pacf_ci[j]:
            j += 1
            if j == nlags:
                break
        #print('choose PACF of order: ', j)

        #get best ACF:
        k = 0
        while not lower_acf_ci[k] <= acf_elements[k] <= upper_acf_ci[k]:
            k += 1
            if k == nlags:
                break
        #print('choose ACF of order: ', k)

        return j, k

if __name__ == '__main__':

    import modules.prognostic.preprocessing.data_selection as ds
    from modules.prognostic.preprocessing.feature_engineering import FeatureExtraction_RealCapacity
    import modules.prognostic.preprocessing.data_labeling as dl
    import modules.prognostic.preprocessing.battery_wise_processing as bwp
    import modules.prognostic.preprocessing.normalization as nm

    battery = ds.DataSelection_specific(['B0005'])
    fs = FeatureExtraction_RealCapacity()
    data = ds.get_df_for_test(battery, fs)
    data = dl.label_data(data, battery.battery_names)
    data_batterywise = bwp.split_by_battery(data)
    cycle = 80
    batterywise_train, batterywise_test = bwp.split_by_cycle(data_batterywise, cycle)
    battery_id_list = bwp.extract_battery_id(data_batterywise)
    scaler = nm.FeatureScaler_MinMax()
    scaler.origin_data = batterywise_train[0]
    batterywise_train[0] = scaler.scale(batterywise_train[0], ignore_columns=['id', 'rul_label', 'fail_within_10_cycles'])
    batterywise_test[0] = scaler.compute(batterywise_test[0])
    print(scaler.scaler_dict)

    prediction_method = myARIMA(batterywise_train[0], batterywise_test[0], scaler,  x_label=['soh_label_scaled'])
    prediction_method.split_at_cycle = cycle
    prediction_method.train(plot=True)
    result, error_cycle = prediction_method.predict(plot=True)