## Parameter Description

| Variable             | Datentyp                    | Einheit      | Description                                                                                                 | Level |
|----------------------|-----------------------------|--------------|-------------------------------------------------------------------------------------------------------------|-------|
| cycle                | np.array [matlab-struct]    | -            | beinhaltet die Daten sowie Metadaten über Phase, Temperatur und Zeit des Experiments an einer Batterie      | 1     |
| type                 | string                      | -            | definiert welcher Phase die Daten zuzuordnen sind (laden, entladen, Elektrochemische Impedanzspektroskopie) | 2     |
| ambient_temperature  | int                         | Grad Celsius | Umgebungstemperatur des Experiments                                                                         | 2     |
| time                 | [MATLAB-Zeitvektor]         | -            | ein Zeitstempel des Starts des Experiments der jeweiligen Phase                                             | 2     |
| data                 | np.array [matlab-struct]    | -            | beinhaltet alle aufgezeichneten Daten einer Phase                                                           | 2     |
| voltage_measured     | float64                     | Volt         | zum Zeitpunkt t gemessene Spannung an der Batterie (Klemmenspannung)                                        | 3     |
| current_measured     | float64                     | Ampere       | zum Zeitpunkt t gemessene Stromstärke an der Batterie [Ausgangs- oder Eingangsstromstärke]                  | 3     |
| temperature_measured | float64                     | Grad Celsius | zum Zeitpunkt t Temperatur der Batterie                                                                     | 3     |
| current_charge       | float64                     | Ampere       | zum Zeitpunkt t gemessene Stromstärke am Ladegerät - während Ladevorgang                                    | 3     |
| voltage_charge       | float64                     | Volt         | zum Zeitpunkt t die Spannung am Ladegerät - während Ladevorgang                                             | 3     |
| current_load         | float64                     | Ampere       | zum Zeitpunkt t gemessene Stromstärke am Verbraucher - während Entladevorgang                               | 3     |
| voltage_load         | float64                     | Volt         | zum Zeitpunkt t die Spannung die am Verbraucher anliegt - während Entladevorgeng                            | 3     |
| time (t)             | float64                     | Sekunden     | Zeitpunkt t ab beginn des Experiments                                                                       | 3     |
| capacity             | float64                     | Amperstunde  | gemessene Kapazität der Batterie bei Entladung                                                              | 3     |
| sense_current        | float64 - komplex           | Ampere       | Sense-stromstärke ???                                                                                       | 3     |
| battery_current      | float64 - komplex           | Ampere       | Stromstärke des Batteriezweigs                                                                              | 3     |
| current_ratio        | float64 - komplex           | -            | Verhältnis der Stromstärken (Sense/Batterie)                                                                | 3     |
| battery_impedance    | float64 - komplex           | Ohm          | aus Rohdaten berechnete Batterieimpedanz                                                                    | 3     |
| rectified_impedance  | float64 - komplex           | Ohm          | kalibrierte und geglättete Batterieimpedanz                                                                 | 3     |
| re                   | float64                     | Ohm          | geschätzter Elektrolytwiderstand                                                                            | 3     |
| rct                  | float64                     | Ohm          | geschätzter Ladungsübertragungswiderstand                                                                   | 3     |



## nested structure of .mat files
```
-- [dict] --> header
          |
          --> version
          |
          --> global
          |
          --> cycle -- [np.array] --> type [charge, discharge and impedance operations]
                                  |
                                  --> ambient_temperature
                                  |
                                  --> time (stamp)
                                  |
                                  --> data -- [np.array] for charge and discharge:
                                                         --> voltage_measured
                                                         |
                                                         --> current_measured
                                                         |
                                                         --> temperature_measured
                                                         |
                                                         --> current_charge / current_load
                                                         |
                                                         --> voltage_charge / current_load
                                                         |
                                                         --> time 
                                                         
                                                         additionaly for discharge:
                                                         |
                                                         --> capacity
                                                         
                                                         for impedance:
                                                         --> sense_current
                                                         |
                                                         --> battery_current
                                                         |
                                                         --> current_ratio
                                                         |
                                                         --> battery_impedance
                                                         |
                                                         --> rectified_impedance
                                                         |
                                                         --> re 
                                                         |
                                                         --> rct
                                                         
```

