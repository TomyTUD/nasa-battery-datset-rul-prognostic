# research work on the NASAs battery dataset

This project contains the progress of analyzing the NASAs battery dataset. Therefore i built a pipeline to investigate different health index extrapolation approaches with python.
The goal is to evaluate the approaches on the accuracy of the remaining useful life prognosis.
I used the batterys B0005, B0006, B0007, B0018. The `feature_tables folder ` allready include the extracted features for the rul prediction.

If you want to import the nasa battery `.mat` files from your local machine you have to set the directory in the `modules/path_of_dataset.py` file.

The code provides the benchmark framework (pipeline), a module to import the matlab-struct files into a python dict and a gui for simple plots. 

- for my research i chose the Nasa Battery Data-Set:
  - [Battery Dataset](https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/)
  - [Dataset Description](https://data.nasa.gov/dataset/Li-ion-Battery-Aging-Datasets/uj5r-zjdb)



- code is based the following libraries: 
  - numpy
  - pandas
  - sklearn
  - statsmodels
  - scipy
  - pyemd
  - pykernels
  - pyswarm
  - matplotlib.